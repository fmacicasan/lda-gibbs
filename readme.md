LDA Gibbs implementation used as part of Octavian Hasna & Florin Macicasan's license thesis.

Based on the implementation of [27] Xuan-Hieu Phan and Cam-Tu Nguyen. (2008) JGibbLDA: A Java implementation of latent Dirichlet allocation (LDA). [Online]. http://jgibblda.sourceforge.net/


