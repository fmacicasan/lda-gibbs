package ro.consys.algorithm.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.consys.algorithm.domain.ExtraWord;
import ro.consys.algorithm.exception.ClassifierException;
import ro.consys.algorithm.exception.ExtracterException;
import ro.consys.algorithm.util.Log;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component("flow")
public class Flow
{
	private static final Logger log = Logger.getLogger(Flow.class);

	@Autowired
	private TopicIdentifierService ldaInferer;

	public Flow()
	{
		log.debug("initializing flow!");
	}


	public List<String> recommend(String url,  // List<String> history,
										 int numberOfAds) throws ExtracterException, ClassifierException
	{
		Log.info(log, "Received", numberOfAds, "recommendation request for", url);
		// TODO: put somewhere here the interaction identifier
		// keyword extraction
		List<ExtraWord> keywords = Collections.emptyList();
		Log.info(log, "Extracted", "keywords from", url);
		// topic extraction
		Map<Integer, Double> topics = ldaInferer.extractTopics(keywords);
		Log.info(log, "infered the topic distribution");
		// annotates ads with contextual and behavioral score
		return Collections.emptyList();
	}
}
