
package ro.consys.algorithm.service.topic.lda.model;


public class Pair implements Comparable<Pair> {
	private Object first;
	private Comparable second;
	public static boolean naturalOrder = false;
	
	
	/************************************************************* from initial [27] implementation **************************************/
	
	public Pair(Object k, Comparable v){
		setFirst(k);
		setSecond(v);		
	}
	
	public Pair(Object k, Comparable v, boolean naturalOrder){
		setFirst(k);
		setSecond(v);
		Pair.naturalOrder = naturalOrder; 
	}
	
	public int compareTo(Pair p){
		if (naturalOrder){
			return this.getSecond().compareTo(p.getSecond());
		} else {
			return -this.getSecond().compareTo(p.getSecond());
		}
	}

	public Object getFirst() {
		return first;
	}

	public void setFirst(Object first) {
		this.first = first;
	}

	public Comparable getSecond() {
		return second;
	}

	public void setSecond(Comparable second) {
		this.second = second;
	}
}

