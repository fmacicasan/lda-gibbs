package ro.consys.algorithm.service.topic.lda.exception;

/**
 * 
 * @author Flopi
 *
 */
public class LDAException extends RuntimeException {
	
	public LDAException(){
		super();
	}
	
	public LDAException(String msg){
		super(msg);
	}
	
	public LDAException(String msg, Throwable e){
		super(msg, e);
	}
	
	public LDAException(Throwable e){
		super(e);
	}

}
