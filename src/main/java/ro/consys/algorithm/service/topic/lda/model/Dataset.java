package ro.consys.algorithm.service.topic.lda.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import ro.consys.algorithm.domain.ExtraWord;
import ro.consys.algorithm.domain.Word;


public class Dataset {
	
	private Dictionary localDict;			// local dictionary	
	private Document [] docs; 		// a list of documents	
	private int M; 			 		// number of documents
	private int V;			 		// number of words
	
	// map from local coordinates (id) to global ones 
	// null if the global dictionary is not set
	private Map<Integer, Integer> lid2gid; 
	
	//link to a global dictionary (optional), null for train data, not null for test data
	private Dictionary globalDict;	 		

//	public LDADataset(){
//		localDict = new Dictionary();
//		M = 0;
//		V = 0;
//		docs = null;
//	
//		globalDict = null;
//		lid2gid = null;
//	}
	
	public Dataset(int M){
		localDict = new Dictionary();
		this.M = M;
		this.V = 0;
		docs = new Document[M];	
		
		globalDict = null;
		lid2gid = null;
	}
	
	public Dataset(int M, Dictionary globalDict){
		localDict = new Dictionary();	
		this.M = M;
		this.V = 0;
		docs = new Document[M];	
		
		this.globalDict = globalDict;
		lid2gid = new HashMap<Integer, Integer>();
	}
	
	/**
	 * set the document at the index idx if idx is greater than 0 and less than M
	 * @param doc document to be set
	 * @param idx index in the document array
	 */	
	public void setDoc(Document doc, int idx){
		if (0 <= idx && idx < M){
			docs[idx] = doc;
		}
	}
	/**
	 * set the document at the index idx if idx is greater than 0 and less than M
	 * @param str string contains doc
	 * @param idx index in the document array
	 */
	public void setDoc(String str, int idx){
		if (0 <= idx && idx < M){
			String [] words = str.split("[ \\t\\n]");
			
			setDoc(str, idx, words);			
		}
	}

	public void setDoc(String rawString, int idx, String[] words) {
		Vector<Integer> ids = new Vector<Integer>();
		
		for (String word : words){
			int newValIfNotPresent = localDict.getWord2IdSize();
			int _id = processWord(word,newValIfNotPresent);
			
			if(_id != -1){
				ids.add(_id);
			}
		}
		
		Document doc = new Document(ids, rawString);
		docs[idx] = doc;
		V = localDict.getWord2IdSize();
	}
	


	public int processWord(String word, int newValIfNotPresent) {
		int _id = newValIfNotPresent;
		
		if (localDict.contains(word)){		
			_id = localDict.getID(word);
		}
						
		if (globalDict != null){
			//get the global id					
			Integer id = globalDict.getID(word);
			//System.out.println(id);
			
			if (id != null){
				localDict.addWord(word);
				
				lid2gid.put(_id, id);
//					ids.add(_id);
			} else { //not in global dictionary
				//do nothing currently
				_id = -1;
			}
		} else {
			localDict.addWord(word);
//				ids.add(_id);
		}
		return _id;
	}
	
	public Dictionary getLocalDictionary(){
		return this.localDict;
	}
	
	public void setLocalDictionary(Dictionary localDict){
		this.localDict = localDict;
	}
	
	public Document getDoc(int doc){
		return docs[doc];
	}
	
	public int getDocLength(int doc){
		return docs[doc].getLength();
	}
	
	public int getWordInDocument(int doc, int word){
		return docs[doc].getWord(word);
	}
	
	public int getNumberOfDocuments(){
		return M;
	}
	
	public int getNumberOfWords(){
		return V;
	}
	
	public void setNumberOfWords(int wordCount){
		this.V = wordCount;
	}
	
	public int getGlobalId(int localId){
		return lid2gid.get(localId);
	}
	
	public Dictionary getGlobalDictionary(){
		return globalDict;
	}
	

	//will have a multiple occurances word prezent more times in the strings collection
	public Set<Word> setDocumentFromStrings(String string, Integer key, List<String> strings) {
		List<Integer> ids = new ArrayList<Integer>();
		Set<Word> wordsToUpdateDictionaryKey = new HashSet<Word>();
		
		for(String word : strings){
			int newValIfNotPresent = localDict.getWord2IdSize();
			int _id = processWord(word,newValIfNotPresent);
			ids.add(_id);
		}
		Document doc = new Document(ids, string);
		docs[key] = doc;
		V = localDict.getWord2IdSize();
		return wordsToUpdateDictionaryKey;
	}
	
	/**
	 * Sets the document with the given id and words
	 * @param rawString some string identifier
	 * @param idx its identifier in the document set
	 * @param words the associated {@link ExtraWord}, will appear {@link ExtraWord#getOccurance()} in the resulting lda document
	 * @return the words that were newly added to the local dictionary
	 */
	public Set<Word> setDocument(String rawString, int idx, Set<ExtraWord> words){
		List<Integer> ids = new ArrayList<Integer>();
		Set<Word> wordsToUpdateDictionaryKey = new HashSet<Word>();
		
		for(ExtraWord word : words){
			//TODO test this, maybe not needed and relax the for bellow
			if(word.getOccurrance() > 0){
				int newValIfNotPresent = localDict.getWord2IdSize();
				int _id = processWord(word.getWord().getName(),newValIfNotPresent);
				if(_id != -1){
					if(newValIfNotPresent == _id){
						word.getWord().setDictionaryKey(_id);
						wordsToUpdateDictionaryKey.add(word.getWord());
					}
					//use the extra word for as many times as he has occurrences (for LDA)
					for(int i=0;i<word.getOccurrance();i++){
						ids.add(_id);
					}
				}
			}
		}
		Document doc = new Document(ids, rawString);
		docs[idx] = doc;
		V = localDict.getWord2IdSize();
		return wordsToUpdateDictionaryKey;
	}
	

	/************************************************************* from initial [27] implementation **************************************/
	/**
	 *  read a dataset from a stream, create new dictionary
	 *  @return dataset if success and null otherwise
	 */
	public static Dataset readDataSet(String filename){
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(filename), "UTF-8"));
			
			Dataset data = readDataSet(reader);
			
			reader.close();
			return data;
		}
		catch (Exception e){
			System.out.println("Read Dataset Error: " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * read a dataset from a file with a preknown vocabulary
	 * @param filename file from which we read dataset
	 * @param dict the dictionary
	 * @return dataset if success and null otherwise
	 */
	public static Dataset readDataSet(String filename, Dictionary dict){
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(filename), "UTF-8"));
			Dataset data = readDataSet(reader, dict);
			
			reader.close();
			return data;
		}
		catch (Exception e){
			System.out.println("Read Dataset Error: " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 *  read a dataset from a stream, create new dictionary
	 *  @return dataset if success and null otherwise
	 */
	public static Dataset readDataSet(BufferedReader reader){
		try {
			//read number of document
			String line;
			line = reader.readLine();
			int M = Integer.parseInt(line);
			
			Dataset data = new Dataset(M);
			for (int i = 0; i < M; ++i){
				line = reader.readLine();
				
				data.setDoc(line, i);
			}
			
			return data;
		}
		catch (Exception e){
			System.out.println("Read Dataset Error: " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * read a dataset from a stream with respect to a specified dictionary
	 * @param reader stream from which we read dataset
	 * @param dict the dictionary
	 * @return dataset if success and null otherwise
	 */
	public static Dataset readDataSet(BufferedReader reader, Dictionary dict){
		try {
			//read number of document
			String line;
			line = reader.readLine();
			int M = Integer.parseInt(line);
			System.out.println("NewM:" + M);
			
			Dataset data = new Dataset(M, dict);
			for (int i = 0; i < M; ++i){
				line = reader.readLine();
				data.setDoc(line, i);
			}
			
			return data;
		}
		catch (Exception e){
			System.out.println("Read Dataset Error: " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * read a dataset from a string, create new dictionary
	 * @param str String from which we get the dataset, documents are seperated by newline character 
	 * @return dataset if success and null otherwise
	 */
	public static Dataset readDataSet(String [] strs){
		Dataset data = new Dataset(strs.length);
		
		for (int i = 0 ; i < strs.length; ++i){
			data.setDoc(strs[i], i);
		}
		return data;
	}
	
	/**
	 * read a dataset from a string with respect to a specified dictionary
	 * @param str String from which we get the dataset, documents are seperated by newline character	
	 * @param dict the dictionary
	 * @return dataset if success and null otherwise
	 */
	public static Dataset readDataSet(String [] strs, Dictionary dict){
		//System.out.println("readDataset...");
		Dataset data = new Dataset(strs.length, dict);
		
		for (int i = 0 ; i < strs.length; ++i){
			//System.out.println("set doc " + i);
			data.setDoc(strs[i], i);
		}
		return data;
	}

	/**
	 * read a single document
	 * @param raw raw text / identifier
	 * @param words words in a document
	 * @param dict source dictionary
	 * @return
	 */
	public static Dataset readDataSet(String raw, String[] words, Dictionary dict) {
		Dataset data = new Dataset(1,dict);
		data.setDoc(raw, 0, words);
		return data;
		
		
	}
}
