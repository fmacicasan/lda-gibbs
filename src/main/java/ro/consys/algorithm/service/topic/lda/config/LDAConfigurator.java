package ro.consys.algorithm.service.topic.lda.config;

import ro.consys.algorithm.service.topic.lda.util.LDACmdOption;
import static ro.consys.algorithm.util.PropertiesUtil.*;

/**
 * 
 * @author Flopi
 *
 */
public class LDAConfigurator {
	
	private ModelStatus modelType = ModelStatus.getModelStatus(getMessage(LDA_INF_MODEL_TYPE));
	
	private String dir = getMessage(LDA_INF_DIR);
	
	private String dfile = getMessage(LDA_INF_DATA_FILE);
	
	private String modelName = getMessage(LDA_INF_MODEL_NAME);
	
	private double alpha = getDouble(LDA_INF_HP_ALHPA);
	
	private double beta = getDouble(LDA_INF_HP_BETA);;
	
	private int K = getInteger(LDA_INF_TOPIC_CNT);
	
	private int niters = getInteger(LDA_INF_ITERATION_CNT);
	
	private int savestep = getInteger(LDA_INF_SAVE_STEP);
	
	private int twords = getInteger(LDA_INF_TOP_WORDS);
	
	private boolean withrawdata = getBoolean(LDA_INF_WITHDRAW_DATA);
	
	private String wordMapFileName = getMessage(LDA_INF_WORDMAP_NAME);
	
	private boolean noprint = getBoolean(LDA_INF_NO_PRINT);
	
	private boolean doParallel = getBoolean(LDA_INF_DO_PARALLEL);
	
	private int threadCnt = getInteger(LDA_INF_THREAD_CNT);
	
	private int repetition = getInteger(LDA_INF_OVERALL_REPETITIONS);
	
	private LDAConfigurator(){
	}
	
	private LDAConfigurator(LDACmdOption cmdOption){
		ModelStatus modelStatus;
		if(cmdOption.est) {
			modelStatus = ModelStatus.EST;
		} else {
			if(cmdOption.estc){
				modelStatus = ModelStatus.ESTC;
			} else {
				//default
				modelStatus = ModelStatus.INF;
			}
		}
		this.modelType = modelStatus;
		this.dir = cmdOption.dir;
		this.dfile = cmdOption.dfile;
		this.modelName = cmdOption.modelName;
		this.alpha = cmdOption.alpha;
		this.beta = cmdOption.beta;
		this.K = cmdOption.K;
		this.niters = cmdOption.niters;
		this.savestep = cmdOption.savestep;
		this.withrawdata = cmdOption.withrawdata;
		this.wordMapFileName = cmdOption.wordMapFileName;
		this.noprint = cmdOption.noprint;
		this.doParallel = cmdOption.doParallel;
		this.threadCnt = cmdOption.threadCnt;
		this.repetition = cmdOption.repetition;
        this.twords = cmdOption.twords;
	}
	
	protected LDAConfigurator(ModelStatus modelType, String dir, String dfile,
			String modelName, double alpha, double beta, int k, int niters,
			int savestep, boolean withrawdata, String wordMapFileName,
			boolean noprint, boolean doParallel, int threadCnt, int repetition, int tword) {
		super();
		this.modelType = modelType;
		this.dir = dir;
		this.dfile = dfile;
		this.modelName = modelName;
		this.alpha = alpha;
		this.beta = beta;
		this.K = k;
		this.niters = niters;
		this.savestep = savestep;
		this.withrawdata = withrawdata;
		this.wordMapFileName = wordMapFileName;
		this.noprint = noprint;
		this.doParallel = doParallel;
		this.threadCnt = threadCnt;
		this.repetition = repetition;
        this.twords = tword;
	}

	public ModelStatus getModelType(){
		return this.modelType;
	}

	public String getDir() {
		return dir;
	}

	public String getDfile() {
		return dfile;
	}

	public String getModelName() {
		return modelName;
	}

	public double getAlpha() {
		return alpha;
	}

	public double getBeta() {
		return beta;
	}

	/**
	 * 
	 * @return the topic count for the model
	 */
	public int getK() {
		return K;
	}

	public int getNiters() {
		return niters;
	}

	public int getSavestep() {
		return savestep;
	}

	public int getTwords() {
		return twords;
	}

	public boolean isWithrawdata() {
		return withrawdata;
	}

	public String getWordMapFileName() {
		return wordMapFileName;
	}

	public boolean isNoprint() {
		return noprint;
	}

	public boolean isDoParallel() {
		return doParallel;
	}

	public int getThreadCnt() {
		return threadCnt;
	}

	public int getRepetition() {
		return repetition;
	}

	@Override
	public String toString() {
		return "LDAConfigurator [modelType=" + modelType + ", dir=" + dir
				+ ", dfile=" + dfile + ", modelName=" + modelName + ", alpha="
				+ alpha + ", beta=" + beta + ", K=" + K + ", niters=" + niters
				+ ", savestep=" + savestep + ", twords=" + twords
				+ ", withrawdata=" + withrawdata + ", wordMapFileName="
				+ wordMapFileName + ", noprint=" + noprint + ", doParallel="
				+ doParallel + ", threadCnt=" + threadCnt + ", repetition="
				+ repetition + "]";
	}
	
	
	
}
