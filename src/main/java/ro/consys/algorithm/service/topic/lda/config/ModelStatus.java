package ro.consys.algorithm.service.topic.lda.config;

import java.util.HashMap;
import java.util.Map;

import ro.consys.algorithm.util.PropertiesUtil;

/**
 * 
 * @author Flopi
 *
 */
public enum ModelStatus {
	
	
    UNKNOWN("unknown"), 
    EST(PropertiesUtil.EST_NAME),
    ESTC(PropertiesUtil.ESTC_NAME), 
    INF(PropertiesUtil.INF_NAME);
    
    private final String value;
    
    private ModelStatus(String name){
    	this.value = name;
    }
    
    public String getValue(){
    	return this.value;
    }
    
    private static Map<String, ModelStatus> valueToModelStatus = new HashMap<String, ModelStatus>();
    
    static{
    	for(ModelStatus modelStatus : ModelStatus.values()){
    		valueToModelStatus.put(modelStatus.getValue(), modelStatus);
    	}
    }
    
    public static ModelStatus getModelStatus(String value){
    	ModelStatus status = valueToModelStatus.get(value);
    	if(status == null){
    		return ModelStatus.UNKNOWN;
    	}
    	return status;
    }
    
    
    
}
