package ro.consys.algorithm.service.topic.distance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * TODO: modify not to copmmunicate via files.
 * @author Flopi
 *
 */
public class SimilarityMesurer {
	
	private HellingerDistance hellingerDistance = new HellingerDistance();
	
	public double[][] readDoubleMatrixFromFile(String filename, int width, int height) throws IOException{
//		if(!filename.startsWith(File.separator)){
//			filename = File.separator + filename;
//		}
//		Reader r = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(filename)));
		Reader r = new BufferedReader(new FileReader(filename));
		StreamTokenizer stok = new StreamTokenizer(r);
		stok.nextToken();
		double[][] numbers = new double[height][width];
		
		for(int i=0;i<height;i++){
			for(int j=0;j<width;j++){
				if(stok.ttype == StreamTokenizer.TT_NUMBER){
					numbers[i][j] = stok.nval;
				}
				stok.nextToken();
			}
		}
		return numbers;
	}
	
	public void computeSimilarity(String advertisementMatrix, String contextMatrix, int topicCnt, int adsCnt, int contextCnt) throws IOException{
		double[][] adsMatrix = readDoubleMatrixFromFile(advertisementMatrix, topicCnt, adsCnt);
		double[][] ctxMatrix = readDoubleMatrixFromFile(contextMatrix, topicCnt, contextCnt);
		
		for(int i=0;i<contextCnt;i++){
			List<RatedAdvertisement> ratedAds = new ArrayList<RatedAdvertisement>();
			for(int j=0;j<adsCnt;j++){
				Double distance = hellingerDistance.hellinger11(ctxMatrix[i], adsMatrix[j]);
				ratedAds.add(new RatedAdvertisement(j, distance));
			}
			Collections.sort(ratedAds);
			System.out.println("Best for context "+i+" are "+ratedAds);//.subList(0, 10)
		}	
	}
	
	public static void main(String[] args) throws IOException{
		SimilarityMesurer sm = new SimilarityMesurer();
//		sm.computeSimilarity("src\\main\\resources\\models\\keycatcs2_rerun2\\ads_keys.dat.model-final.theta", "src\\main\\resources\\models\\keycatcs2_rerun2\\testPagesKeywords2.dat.model-final.theta", 50, 1937, 57);
		sm.computeSimilarity("src\\main\\resources\\models\\keycatcs2_rerun2\\ads_keys.dat.model-final.theta", "src\\main\\resources\\models\\keycatcs2_rerun2\\ads_keys.dat.model-final.theta", 50, 1937, 57);
//		sm.computeSimilarity("src\\main\\resources\\models\\keycatcs2_rerun2\\testPagesKeywords2.dat.model-final.theta", "src\\main\\resources\\models\\keycatcs2_rerun2\\testPagesKeywords2.dat.model-final.theta", 50, 57, 57);
		//sm.computeSimilarity("src\\main\\resources\\models\\keycatcs2_rerun\\model-final.theta", "src\\main\\resources\\models\\keycatcs2_rerun\\testPagesKeywords.dat.model-final.theta", 50, 1937, 13);
	}
}

class RatedAdvertisement implements Comparable<RatedAdvertisement> {
	private int advertisementId;
	private Double rating;
	
	public RatedAdvertisement(final int advertisementId,final double rating){
		this.advertisementId = advertisementId;
		this.rating = rating;
	}

	//for DESC ordering
	public int compareTo(RatedAdvertisement o) {
		return rating.compareTo(o.getRating());//(-1) * 
	}
	
	public Double getRating(){
		return this.rating;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(advertisementId).append("_").append(rating).append(" ");
		return sb.toString();
	}
}
