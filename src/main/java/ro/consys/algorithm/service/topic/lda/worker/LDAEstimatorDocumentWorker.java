package ro.consys.algorithm.service.topic.lda.worker;

import ro.consys.algorithm.service.topic.lda.Estimator;
import ro.consys.algorithm.service.topic.lda.TopicModel;

/**
 * 
 * @author Flopi
 *
 */
public class LDAEstimatorDocumentWorker extends LDAParallelWorker {
	
	private Estimator estimator;
//	private Model trnModel;
//	
//	private int threadCnt;
//	private int alfa;
	
	public LDAEstimatorDocumentWorker(Estimator estimator, TopicModel trnModel, int threadCnt, int alfa) {
		super(trnModel, threadCnt, alfa);
		this.estimator = estimator;
//		this.trnModel = trnModel;
//		this.threadCnt = threadCnt;
//		this.alfa = alfa;
	}

	public void run() {
		
		//int inc = 1;
		String me = Thread.currentThread().getName();
		int m2 = model.getDatasetSize();
		int start = rank*m2/threadCnt;
		int end = (rank+1)*m2/threadCnt;
		double [] partialTop = new double[model.getTopicCount()];
//		System.out.println(me + "start "+start+" end "+end+" max"+m2);
		for (int m = start; m < end; m++){				
			for (int n = 0; n < model.getDocLength(m); n++){
				// z_i = z[m][n]
				// sample from p(z_i|z_-i, w)
				int topic = estimator.samplingSynchronized(m, n,partialTop);
//				int topic = estimator.samplingSafe(m, n,p);
//				int topic = estimator.sampling(m, n,p);
				model.setTopicOfWordInDocument(m, n, topic);
//				trnModel.z[m].set(n, topic);
			}// end for each word
		}// end for each document
		
	}

}
