package ro.consys.algorithm.service.topic.lda.model;

import java.util.List;
import java.util.Vector;

import ro.consys.algorithm.service.topic.lda.exception.LDAException;

public class Document {

	private int [] words;
	private String rawStr;
	private int length;
	
	public Document(int[] words, String rawStr, int length){
		this.words = words;
		this.rawStr = rawStr;
		this.length = length;
	}
	
	public Document(){
		this(new int[0], "", 0);
	}
	
	public Document(int length){
		this(new int[length], "", length);
	}
	
	/**
	 * The words reference is used directly (no deep copy made)
	 * @param length
	 * @param words
	 */
	public Document(int length, int [] words){
		this(words,"", length);
	}
	
	public Document(int length, int [] words, String rawStr){
		this(words, rawStr, length);
	}
	
	public Document(List<Integer> doc, String rawStr){
		this.length = doc.size();
		this.rawStr = rawStr;
		this.words = new int[length];
		for (int i = 0; i < length; ++i){
			this.words[i] = doc.get(i);
		}
	}
	
	public int getWord(int n){
		if(n < 0 || n >= words.length){
			throw new LDAException("invalid word index:"+n);
		}
		return words[n];
	}
	
	public int getLength(){
		return this.length;
	}
	
	
	/************************************************************* from initial [27] implementation **************************************/
	
	public Document(Vector<Integer> doc){
		this(doc, "");
	}
	
	public Document(Vector<Integer> doc, String rawStr){
		this.length = doc.size();
		this.rawStr = rawStr;
		this.words = new int[length];
		for (int i = 0; i < length; ++i){
			this.words[i] = doc.get(i);
		}
	}
	

}
