package ro.consys.algorithm.service.topic.repoitory;

import org.springframework.data.jpa.domain.Specification;
import ro.consys.algorithm.domain.Topic;

/**
 * @author flo
 * @since 20/03/16.
 */
public interface TopicRepository {
    Topic findOne(Specification<Topic> topicSpecification);
}
