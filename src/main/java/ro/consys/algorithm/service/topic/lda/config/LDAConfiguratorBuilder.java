package ro.consys.algorithm.service.topic.lda.config;

import static ro.consys.algorithm.util.PropertiesUtil.*;
import ro.consys.algorithm.service.topic.lda.util.LDACmdOption;

/**
 * 
 * @author Flopi
 *
 */
public class LDAConfiguratorBuilder implements LDABuildable<LDAConfigurator> {

    private ModelStatus modelType;
    private String dir;
    private String dfile;
    private String modelName;
    private double alpha;
    private double beta;
    private int k;
    private int niters;
    private int savestep;
    private boolean withrawdata;
    private String wordMapFileName;
    private boolean noprint;
    private boolean doParallel;
    private int threadCnt;
    private int repetition;
    private int tword;

    private LDAConfiguratorBuilder(){
        //cannot be externally instantiated
    }
    
    public static LDAConfiguratorBuilder map(LDACmdOption cmdOption){
    	LDAConfiguratorBuilder builder = new LDAConfiguratorBuilder();
    	ModelStatus modelStatus;
		if(cmdOption.est) {
			modelStatus = ModelStatus.EST;
		} else {
			if(cmdOption.estc){
				modelStatus = ModelStatus.ESTC;
			} else {
				//default
				modelStatus = ModelStatus.INF;
			}
		}
		builder.setModelType(modelStatus);
		builder.setDir(cmdOption.dir);
		builder.setDfile(cmdOption.dfile);
		builder.setModelName(cmdOption.modelName);
		builder.setAlpha(cmdOption.alpha);
		builder.setBeta(cmdOption.beta);
		builder.setK(cmdOption.K);
		builder.setNiters(cmdOption.niters);
		builder.setSavestep(cmdOption.savestep);
		builder.setWithrawdata(cmdOption.withrawdata);
		builder.setWordMapFileName(cmdOption.wordMapFileName);
		builder.setNoprint(cmdOption.noprint);
		builder.setDoParallel(cmdOption.doParallel);
		builder.setThreadCnt(cmdOption.threadCnt);
		builder.setRepetition(cmdOption.repetition);
        builder.setTWords(cmdOption.twords);
    	return builder;
    }

    public static LDAConfiguratorBuilder defaults(){
        LDAConfiguratorBuilder builder = new LDAConfiguratorBuilder();
        builder.setModelType(ModelStatus.getModelStatus(getMessage(LDA_INF_MODEL_TYPE)));
        builder.setDir(getMessage(LDA_INF_DIR));
        builder.setDfile(getMessage(LDA_INF_DATA_FILE));
        builder.setModelName(getMessage(LDA_INF_MODEL_NAME));
        builder.setAlpha(getDouble(LDA_INF_HP_ALHPA));
        builder.setBeta(getDouble(LDA_INF_HP_BETA));
        builder.setK(getInteger(LDA_INF_TOPIC_CNT));
        builder.setNiters(getInteger(LDA_INF_ITERATION_CNT));
        builder.setSavestep(getInteger(LDA_INF_SAVE_STEP));
        builder.setTWords(getInteger(LDA_INF_TOP_WORDS));
        builder.setWithrawdata(getBoolean(LDA_INF_WITHDRAW_DATA));
        builder.setWordMapFileName(getMessage(LDA_INF_WORDMAP_NAME));
        builder.setNoprint(getBoolean(LDA_INF_NO_PRINT));
        builder.setDoParallel(getBoolean(LDA_INF_DO_PARALLEL));
        builder.setThreadCnt(getInteger(LDA_INF_THREAD_CNT));
        builder.setRepetition(getInteger(LDA_INF_OVERALL_REPETITIONS));
        return builder;
    }

    public LDAConfiguratorBuilder setModelType(ModelStatus modelType) {
        this.modelType = modelType;
        return this;
    }

    public LDAConfiguratorBuilder setDfile(String dfile) {
        this.dfile = dfile;
        return this;
    }

    public LDAConfiguratorBuilder setTWords(Integer tWord) {
        this.tword = tWord;
        return this;
    }


    public LDAConfiguratorBuilder setDir(String dir) {
        this.dir = dir;
        return this;
    }


    public LDAConfiguratorBuilder setModelName(String modelName) {
        this.modelName = modelName;
        return this;
    }

    public LDAConfiguratorBuilder setAlpha(double alpha) {
        this.alpha = alpha;
        return this;
    }

    public LDAConfiguratorBuilder setBeta(double beta) {
        this.beta = beta;
        return this;
    }

    public LDAConfiguratorBuilder setK(int k) {
        this.k = k;
        return this;
    }

    public LDAConfiguratorBuilder setNiters(int niters) {
        this.niters = niters;
        return this;
    }

    public LDAConfiguratorBuilder setSavestep(int savestep) {
        this.savestep = savestep;
        return this;
    }

    public LDAConfiguratorBuilder setWithrawdata(boolean withrawdata) {
        this.withrawdata = withrawdata;
        return this;
    }

    public LDAConfiguratorBuilder setWordMapFileName(String wordMapFileName) {
        this.wordMapFileName = wordMapFileName;
        return this;
    }

    public LDAConfiguratorBuilder setNoprint(boolean noprint) {
        this.noprint = noprint;
        return this;
    }

    public LDAConfiguratorBuilder setDoParallel(boolean doParallel) {
        this.doParallel = doParallel;
        return this;
    }

    public LDAConfiguratorBuilder setThreadCnt(int threadCnt) {
        this.threadCnt = threadCnt;
        return this;
    }

    public LDAConfiguratorBuilder setRepetition(int repetition) {
        this.repetition = repetition;
        return this;
    }

    public LDAConfigurator get() {
        return new LDAConfigurator(modelType, dir, dfile, modelName, alpha, beta, k
        ,niters, savestep, withrawdata, wordMapFileName, noprint, doParallel, threadCnt, repetition,tword);
    }
}