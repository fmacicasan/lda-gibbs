package ro.consys.algorithm.service.topic.lda.worker;

import ro.consys.algorithm.service.topic.lda.Inferencer;
import ro.consys.algorithm.service.topic.lda.TopicModel;

/**
 * 
 * @author Flopi
 *
 */
public class LDAInferencerDocumentWorker extends LDAParallelWorker {
	private Inferencer inferator;

	
	public LDAInferencerDocumentWorker(Inferencer inferator, TopicModel newModel, int threadCnt, int alfa) {
		super(newModel,threadCnt, alfa);
		this.inferator = inferator;
	}

	public void run() {

		int datasetSize = model.getDatasetSize();
		int start = rank*datasetSize/threadCnt;
		int end = (rank+1)*datasetSize/threadCnt;
		
		double [] partialTop = new double[model.getTopicCount()];
		for (int newDoc = start; newDoc < end; newDoc++){
			int docLength = model.getDocLength(newDoc);
			for (int wordInDoc = 0; wordInDoc < docLength; wordInDoc++){
				// sample from p(z_i|z_-1,w)
				int topic = inferator.infSamplingSynchronized(newDoc, wordInDoc,partialTop);
				model.setTopicOfWordInDocument(newDoc, wordInDoc, topic);
			}
		}
	}
}


//		String me = Thread.currentThread().getName();
//System.out.println(me + "start "+start+" end "+end+" max"+m2);	
//this.newModel = newModel;
//this.threadCnt = threadCnt;
//this.alfa = alfa;
//private Model newModel;
//
//private int threadCnt;
//private int alfa;
//int inc = 1;