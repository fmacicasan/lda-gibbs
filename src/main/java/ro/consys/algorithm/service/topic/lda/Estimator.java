package ro.consys.algorithm.service.topic.lda;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import ro.consys.algorithm.service.topic.TopicLDAInferenceAdapter;
import ro.consys.algorithm.service.topic.lda.config.LDAConfigurator;
import ro.consys.algorithm.service.topic.lda.config.LDAConfiguratorBuilder;
import ro.consys.algorithm.service.topic.lda.config.ModelStatus;
import ro.consys.algorithm.service.topic.lda.model.Dataset;
import ro.consys.algorithm.service.topic.lda.util.Conversion;
import ro.consys.algorithm.service.topic.lda.util.LDACmdOption;
import ro.consys.algorithm.service.topic.lda.worker.LDAEstimatorDocumentWorker;
import ro.consys.algorithm.util.Log;

/**
 * 
 * @author Flopi
 *
 */
public class Estimator extends LDAProcess {
	private static final Logger log = Logger.getLogger(Estimator.class);	
	
	public Estimator(){
		super();
	}
	
	public Estimator(int threadCount){
		super(threadCount);
	}

	public boolean init(LDACmdOption option) {
//		return init(new LDAConfigurator(option));
		return init(LDAConfiguratorBuilder.map(option).get());
	}
	
	
	public boolean init(LDAConfigurator option){
		this.option = option;
		trainModel = new TopicModel();

		ModelStatus modelType = this.option.getModelType();
		switch(modelType){
			case EST:
				if (!trainModel.initNewModel(this.option)){
					return false;
				}
				trainModel.getLocalDictionary().writeWordMap(option.getDir() + File.separator+ option.getWordMapFileName());
				break;
			case ESTC:
				if (!trainModel.initEstimatedModel(this.option)){
					return false;
				}
				break;
			default:
				Log.error(log,"Invalid model status",modelType);
				return false;
		}

		return true;
	}

	public void estimate() {
		Log.info(log,"Sampling",trainModel.getSamplingIterationCount(),"iteration!");

		int lastIter = trainModel.getSavingIterationCount();
		for (trainModel.setSavingIterationCount(lastIter + 1); trainModel.getSavingIterationCount() < trainModel.getSamplingIterationCount()
				+ lastIter; trainModel.incrementSavingIterationCount()) {
			Log.info(log,"Iteration",trainModel.getSavingIterationCount(),"...");

			// for all z_i

			if (option.isDoParallel()) {
				doWorkOnDocumentsParallel();
			} else {
				doWorkOnDocumentSequential();
			}

			if (option.getSavestep() > 0) {
				if (trainModel.getSavingIterationCount() % option.getSavestep() == 0) {
					Log.info(log,"Saving the model at iteration",trainModel.getSavingIterationCount()," ...");
					saveModel("model-"+ Conversion.ZeroPad(trainModel.getSavingIterationCount(), 5));
				}
			}
		}// end iterations

		if (!option.isNoprint()) {
			System.out.println("Gibbs sampling completed!\n");
			System.out.println("Saving the final model!\n");
			trainModel.decrementSavingIterationCount();
			saveModel("model-final");
		}
	}

	public void saveModel(String name) {
		trainModel.computeTheta();
		trainModel.computePhi();
		trainModel.saveModel(name);
	}

	private void doWorkOnDocumentSequential() {
		double[] p = new double[trainModel.getTopicCount()];
		for (int m = 0; m < trainModel.getDatasetSize(); m++) {
			for (int n = 0; n < trainModel.getDocLength(m); n++) {
				// z_i = z[m][n]
				// sample from p(z_i|z_-i, w)
				int topic = sampling(m, n, p);
				trainModel.setTopicOfWordInDocument(m, n, topic);
			}
		}
	}

	private void doWorkOnDocumentsParallel() {
		List<Future<?>> runners = new ArrayList<Future<?>>();
		int threadCount = getThreadCount();
		for (int i = 0; i < threadCount; i++) {
			runners.add(submit(new LDAEstimatorDocumentWorker(
					this, trainModel, threadCount, i)));
		}
		for (Future<?> future : runners) {
			try {
				future.get();
			} catch (InterruptedException e) {
			} catch (ExecutionException e) {
			}
		}
	}


	public int sampling(int m, int n, double[] p) {
		// remove z_i from the count variable
		int topic = trainModel.getTopicOfWordInDocument(m, n);
		int w = trainModel.getWordInDocument(m, n);

		trainModel.removeOldTopicFromCountables(m, topic, w);

		topic = doEstimation(m, p, w);

		trainModel.addNewTopicToCountables(m, topic, w);
		return topic;
	}

	public int samplingSynchronized(int m, int n, double[] p) {
		// remove z_i from the count variable
		int topic = trainModel.getTopicOfWordInDocument(m, n);
		int w = trainModel.getWordInDocument(m, n);

		// remove some of them from sync block (2 and 4)
		synchronized (trainModel) {
			trainModel.removeOldTopicFromCountables(m, topic, w);
		}

		topic = doEstimation(m, p, w);

		synchronized (trainModel) {
			trainModel.addNewTopicToCountables(m, topic, w);
		}
		return topic;
	}

	public int samplingSafe(int m, int n, double[] p) {
		// remove z_i from the count variable
		// long start = System.nanoTime();
		int topic = trainModel.getTopicOfWordInDocument(m, n);
//		int topic = trnModel.z[m].get(n);
		int w = trainModel.getWordInDocument(m, n);

//		if (trnModel.nw[w][topic] <= 0) {
//			return topic;
//		}
		if (trainModel.getNumberOfWordsAssignedToTopic(w, topic) <= 0) {
			return topic;
		}
		trainModel.removeOldTopicFromCountables(m, topic, w);

		topic = doEstimation(m, p, w);

		trainModel.addNewTopicToCountables(m, topic, w);
		// System.out.println("Inference for "+m+" and "+n+" took "+(System.nanoTime()-start));
		return topic;
	}

	// extract outside
	private int doEstimation(int m, double[] p, int w) {
		int topic;
		double Vbeta = trainModel.getVocabularySize() * trainModel.getEta();
		double Kalpha = trainModel.getTopicCount() * trainModel.getAlpha();

		// do multinominal sampling via cumulative method
		for (int k = 0; k < trainModel.getTopicCount(); k++) {
//			
			p[k] = (
						(trainModel.getNumberOfWordsAssignedToTopic(w, k) + trainModel.getEta())
						/ 
						(trainModel.getTotalNumberOfWordsInTopic(k) + Vbeta)
					)
					* 
					(
						(trainModel.getNumberOfWordsInDocAssignedToTopic(m, k) + trainModel.getAlpha())
						/ 
						(trainModel.getTotalNumberOfWordsInDocument(m) + Kalpha)
					);
		}

		// cumulate multinomial parameters
		for (int k = 1; k < trainModel.getTopicCount(); k++) {
			p[k] += p[k - 1];
		}

		// scaled sample because of unnormalized p[]
		double u = Math.random() * p[trainModel.getTopicCount() - 1];

		for (topic = 0; topic < trainModel.getTopicCount(); topic++) {
			if (p[topic] > u) // sample topic w.r.t distribution p
				break;
		}
		return topic;
	}
	
	/************************************************************* from initial [27] implementation **************************************/
	public boolean init(LDAConfigurator option, Dataset initDataSet){
		this.option = option;
		trainModel = new TopicModel();
		Log.info(log,"initializing new model with dataset");
		if (!trainModel.initNewModel(this.option, initDataSet)){
			return false;
		}
		Log.info(log, "Writing word map");
		trainModel.getLocalDictionary().writeWordMap(option.getDir() + File.separator+ option.getWordMapFileName());
		
		
		return true;
		
	}
}
