package ro.consys.algorithm.service.topic;

import java.util.List;
import java.util.Map;
import java.util.Set;

import ro.consys.algorithm.domain.ExtraWord;
import ro.consys.algorithm.domain.Word;
import ro.consys.algorithm.service.topic.lda.config.LDAConfigurator;

public interface TopicSynchronizationService {

	/**
	 * Creates a new model using the provided configuration and the 
	 * documents represented by the bag of words modeled using the 
	 * set of extra words and their identifier
	 * 
	 * @param ldaConfigurator {@link LDAConfigurator} that holds the 
	 * parameters for LDA configuration
	 * @param trainingDocumentWords a {@link Map} having as key an ordered 
	 * identifier (identifies the document in the trained model) and a 
	 * {@link Set} of {@link ExtraWord} that for the current document
	 * @return a {@link Set} of {@link Word} that need to be persisted / updated
	 */
	public Set<Word> estimateFromScratch(LDAConfigurator ldaConfigurator,
			Map<Integer, Set<ExtraWord>> trainingDocumentWords);

	public Set<Word> estimateFromScratchRaw(LDAConfigurator ldaConfigurator,
			Map<Integer, List<String>> trainingDocumentWords);
}