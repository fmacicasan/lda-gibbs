package ro.consys.algorithm.service.topic.lda.config;

/**
 * 
 * @author Flopi
 *
 */
public interface LDABuildable<T> {
	public T get(); 
}
