package ro.consys.algorithm.service.topic.lda;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import ro.consys.algorithm.service.topic.TopicLDAInferenceAdapter;
import ro.consys.algorithm.service.topic.lda.config.LDAConfigurator;
import ro.consys.algorithm.service.topic.lda.model.Dictionary;
import ro.consys.algorithm.service.topic.lda.model.Dataset;
import ro.consys.algorithm.service.topic.lda.util.LDACmdOption;
import ro.consys.algorithm.service.topic.lda.worker.LDAInferencerDocumentWorker;
import ro.consys.algorithm.util.Log;
import ro.consys.algorithm.util.PropertiesUtil;



public class Inferencer extends LDAProcess {
	private static final Logger log = Logger.getLogger(Inferencer.class);
	
	private Dictionary globalDict;
	
	
	private TopicModel newModel;
	private int niters = PropertiesUtil.getInteger(PropertiesUtil.LDA_INF_ITERATION_CNT);
	
	public Inferencer(int threadCount){
		super(threadCount);
	}
	
	public boolean init(LDAConfigurator configurator){
		this.option = configurator;
		trainModel = new TopicModel();
		
		if (!trainModel.initEstimatedModel(option))
			return false;		
		
		globalDict = trainModel.getLocalDictionary();
		trainModel.computeTheta();
		trainModel.computePhi();
		
		return true;
	}
	
	/**
	 * Constructs a new model for inference
	 * @param newData the new data
	 * @return the new model
	 */
	public TopicModel infer( Dataset newData){

		Log.info(log,"inference from dataset");
		TopicModel newModel = new TopicModel();		
		newModel.initNewModel(option, newData, trainModel);
		
		this.newModel = newModel;	
		
		
		Log.info(log,"Sampling",niters,"iteration for inference!");		
		iterateOverSavingRepetitions(option.isDoParallel());
		
		Log.info(log,"Gibbs sampling for inference completed!");
		
		newModel.computeNewTheta();
		newModel.computeNewPhi(trainModel);
		newModel.decrementSavingIterationCount();
		return this.newModel;
	}
	
	public TopicModel infer(String [] strs){
		Log.info(log,"inference from string");
		
		Dataset dataset = Dataset.readDataSet(strs, globalDict);
		
		return infer(dataset);
	}
	
	public TopicModel infer(String raw, String[] words){
		Dataset dataset = Dataset.readDataSet(raw, words, globalDict);
		return infer(dataset);
	}

	private void iterateOverSavingRepetitions(boolean doParallel) {
		Log.info(log, "Starting the gibbs iterations",doParallel);
		for (newModel.initializeSavingIterationCount(); newModel.getSavingIterationCount() <= niters; newModel.incrementSavingIterationCount()){
			Log.debug(log,"Iteration",newModel.getSavingIterationCount()," ...");
			if(doParallel){
				parallelLDAInference();
			} else {
				doWorkOnDocumentsSequential();
			}
		}
	}

	private void doWorkOnDocumentsSequential() {
		// for all newz_i
		
		double [] p = new double[Math.max(trainModel.getTopicCount(), newModel.getTopicCount())];
		for (int m = 0; m < newModel.getDatasetSize(); ++m){
			for (int n = 0; n < newModel.getDocLength(m); n++){
				// (newz_i = newz[m][n]
				// sample from p(z_i|z_-1,w)
				int topic = infSampling(m, n,p);
				newModel.setTopicOfWordInDocument(m, n, topic);
			}
		}
	}
	
	private void parallelLDAInference() {
		List<Future<?>> runners = new ArrayList<Future<?>>();
		int threadCount = getThreadCount();
		for(int i=0;i<threadCount;i++){
			runners.add(submit(new LDAInferencerDocumentWorker(
					this, newModel, threadCount, i))
					);
		}
		for(Future<?> future : runners){
			try {
				future.get();
			} catch (InterruptedException e) {
			} catch (ExecutionException e) {
			}
		}
	}
	
	/**
	 * do sampling for inference
	 * m: document number
	 * n: word number?
	 */
	public int infSampling(int m, int n, double [] p){
		// remove z_i from the count variables
		int topic = newModel.getTopicOfWordInDocument(m, n);
		int _w = newModel.getWordInDocument(m, n);
		int w = newModel.getGlobalId(_w);
		try
		{
			newModel.removeOldTopicFromCountables(m, topic, _w);
	
			topic = doInference(m, p, _w, w);
			
			newModel.addNewTopicToCountables(m, topic, _w);
		}catch(Exception e){
			Log.error(log, e);
		}
		
		return topic;
	}
//	int topic = newModel.z[m].get(n);
	
	public int infSamplingSynchronized(int document, int word, double [] p){
		
		int topic = newModel.getTopicOfWordInDocument(document, word);
		int wordInDocument = newModel.getWordInDocument(document, word);
		int wordInDocumentGlobaId = newModel.getGlobalId(wordInDocument);
		
		synchronized (newModel) {
			// remove z_i from the count variables
			newModel.removeOldTopicFromCountables(document, topic, wordInDocument);
		}
		topic = doInference(document, p, wordInDocument, wordInDocumentGlobaId);
		synchronized(newModel){
			// add new topic z_inew from the count variables
			newModel.addNewTopicToCountables(document, topic, wordInDocument);
		}
		return topic;
	}
	
	private int doInference(int document, double[] p, int wordInInfModel, int wordInTrainModel) {
		int topic;
		double vEta = trainModel.getVocabularySize() * newModel.getEta();
		double Kalpha = trainModel.getTopicCount() * newModel.getAlpha();
		
		// do multinomial sampling via cummulative method		
		int topicCount = newModel.getTopicCount();
		for (int k = 0; k < topicCount; k++){
			int trainWordsInTopic = trainModel.getNumberOfWordsAssignedToTopic(wordInTrainModel, k);
			int trainTotalWordInTopic = trainModel.getTotalNumberOfWordsInTopic(k);
			int inferenceWordsInTopic = newModel.getNumberOfWordsAssignedToTopic(wordInInfModel,k);
			int inferenceTotalWordInTopic = newModel.getTotalNumberOfWordsInTopic(k);
			int inferenceWordsInDocWithTopic = newModel.getNumberOfWordsInDocAssignedToTopic(document, k);
			int inferenceTotalWordsInDoc = newModel.getTotalNumberOfWordsInDocument(document);
			p[k] = (
						(trainWordsInTopic+ inferenceWordsInTopic + newModel.getEta())
						/
						(trainTotalWordInTopic +  inferenceTotalWordInTopic + vEta)
					)
					*
					(
						(inferenceWordsInDocWithTopic+ newModel.getAlpha())
						/
						(inferenceTotalWordsInDoc + Kalpha)
					);
		}
		// cummulate multinomial parameters
		for (int k = 1; k < topicCount; k++){
			p[k] += p[k - 1];
		}
		
		// scaled sample because of unnormalized p[]
		double u = Math.random() * p[topicCount - 1];
		
		int modifier = 1;
		for (topic = 0; topic < topicCount; topic++){
			if (p[topic] > u){
				modifier = 0;
				break;
			}
		}
		return topic - modifier;
	}
	
	public int infSamplingSafe(int m, int n, double [] p){
		// remove z_i from the count variables
//		int topic = newModel.z[m].get(n);
		int topic = newModel.getTopicOfWordInDocument(m, n);
		int _w = newModel.getWordInDocument(m, n);
		int w = newModel.getGlobalId(_w);
		
		if(newModel.getNumberOfWordsAssignedToTopic(_w, topic) <= 0){
			return topic;
		}
		newModel.removeOldTopicFromCountables(m, topic, _w);

		
		topic = doInference(m, p, _w, w);
		
		newModel.addNewTopicToCountables(m, topic, _w);
		
		return topic;
	}

	public Map<Integer, Double> getTrainingDocumentTopicDistribution(int trainingDocumentIdentifier) {
		if(trainingDocumentIdentifier > trainModel.getDatasetSize()){
			//TODO do validation
		}
		return trainModel.getTopicDistribution(trainingDocumentIdentifier);
	}
	
	/************************************************************* from initial [27] implementation **************************************/
	public TopicModel infer(){	
		System.out.println("inference from file");
		
		newModel = new TopicModel();
		if (!newModel.initNewModel(option, trainModel)) return null;
		
		Log.info(log,"Sampling",niters,"iteration for inference!");
		
		iterateOverSavingRepetitions(option.isDoParallel());
		
		Log.info(log,"Gibbs sampling for inference completed!");
		if(!option.isNoprint()){
			Log.info(log,"Saving the inference outputs!");
			
			newModel.computeNewTheta();
			newModel.computeNewPhi(trainModel);
			newModel.decrementSavingIterationCount();
			newModel.saveModel();
		}
		
		return newModel;
	}
	
}
