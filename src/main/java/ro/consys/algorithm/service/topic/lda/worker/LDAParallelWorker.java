package ro.consys.algorithm.service.topic.lda.worker;

import ro.consys.algorithm.service.topic.lda.TopicModel;

/**
 * 
 * @author Flopi
 *
 */
public abstract class LDAParallelWorker implements Runnable {

	protected TopicModel model;
	
	protected int threadCnt;
	protected int rank;
	
	public LDAParallelWorker(TopicModel model, int threadCnt, int alfa) {
		super();
		this.model = model;
		this.threadCnt = threadCnt;
		this.rank = alfa;
	}

	@Override
	public abstract void run();

//	public TopicModel getModel() {
//		return model;
//	}
//
//	public int getThreadCnt() {
//		return threadCnt;
//	}
//
//	public int getRank() {
//		return rank;
//	}
}
