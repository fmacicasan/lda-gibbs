package ro.consys.algorithm.service.topic;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import ro.consys.algorithm.domain.ExtraWord;
import ro.consys.algorithm.domain.Word;
import ro.consys.algorithm.service.topic.lda.Estimator;
import ro.consys.algorithm.service.topic.lda.config.LDAConfigurator;
import ro.consys.algorithm.service.topic.lda.model.Dataset;
import ro.consys.algorithm.util.Log;

/**
 * 
 * @author Flopi
 *
 */
public class TopicLDAEstimationAdapter implements TopicSynchronizationService {
	private Logger log = Logger.getLogger(TopicLDAEstimationAdapter.class);
	
	private Estimator estiamator;
	
	public TopicLDAEstimationAdapter(){
		estiamator = new Estimator();
		//default lda configurator (from file configs)
	}
	
	private void estimateFromScratch(LDAConfigurator ldaConfigurator, Dataset dataset){
		Log.info(log, "initializing estimator");
		estiamator.init(ldaConfigurator, dataset);
		Log.info(log, "performing estimation");
		estiamator.estimate();
		Log.info(log, "saving the model");
		estiamator.saveModel(ldaConfigurator.getModelName());
	}
	
	@Override
	public Set<Word> estimateFromScratch(LDAConfigurator ldaConfigurator, 
			Map<Integer,Set<ExtraWord>> trainingDocumentWords){
		Log.info(log,"Estimating from scratch with configurator",ldaConfigurator,
				"a number of",trainingDocumentWords.size(),"documents");
		Set<Word> toBeUpdated = new HashSet<Word>();
		
		Dataset dataset = new Dataset(trainingDocumentWords.size());
		for(Map.Entry<Integer, Set<ExtraWord>> entry : trainingDocumentWords.entrySet()){
			Integer key = entry.getKey();
			toBeUpdated.addAll(dataset.setDocument("random text"+key,key, entry.getValue()));
		}
		estimateFromScratch(ldaConfigurator, dataset);
		Log.info(log, "finished estimating from scratch");
		return toBeUpdated;
	}
	
	/**
	 * Uses a list of duplicated words for each document in the traiining set
	 * @param ldaConfigurator
	 * @param trainingDocumentWords
	 * @return
	 */
	@Override
	public Set<Word> estimateFromScratchRaw(LDAConfigurator ldaConfigurator, Map<Integer,List<String>> trainingDocumentWords){
		Log.info(log,"Estimating from scratch with configurator",ldaConfigurator,
				"a number of",trainingDocumentWords.size(),"documents");
		Set<Word> toBeUpdated = new HashSet<Word>();
		Dataset dataset = new Dataset(trainingDocumentWords.size());
		for(Map.Entry<Integer, List<String>> entry : trainingDocumentWords.entrySet()){
			Integer key = entry.getKey();
			toBeUpdated.addAll(dataset.setDocumentFromStrings("random text"+key,key, entry.getValue()));
		}
		estimateFromScratch(ldaConfigurator, dataset);
		Log.info(log, "finished estimating from scratch2");
		return toBeUpdated;
	}
	
	
}
