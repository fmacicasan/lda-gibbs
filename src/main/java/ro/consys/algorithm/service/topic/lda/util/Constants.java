
package ro.consys.algorithm.service.topic.lda.util;

/************************************************************* from initial [27] implementation **************************************/
public class Constants {
	public static final long BUFFER_SIZE_LONG = 1000000;
	public static final short BUFFER_SIZE_SHORT = 512;
	
}
