package ro.consys.algorithm.service.topic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import ro.consys.algorithm.domain.ExtraWord;
import ro.consys.algorithm.domain.LDADocument;
import ro.consys.algorithm.domain.Topic;
import ro.consys.algorithm.domain.specification.TopicSpecification;
import ro.consys.algorithm.service.TopicIdentifierService;
import ro.consys.algorithm.service.topic.lda.Inferencer;
import ro.consys.algorithm.service.topic.lda.TopicModel;
import ro.consys.algorithm.service.topic.lda.config.LDAConfigurator;
import ro.consys.algorithm.service.topic.lda.config.LDAConfiguratorBuilder;
import ro.consys.algorithm.service.topic.repoitory.TopicRepository;

import java.util.*;

/**
 * will use underlying topic model
 * 
 * @author Flopi
 *
 */
@Component("ldaInferencer")
@Configurable
public class TopicLDAInferenceAdapter implements TopicIdentifierService{
private static final String DEFAULT_INFERENCE_NAME = "default instance name";

	
	private Inferencer inferencer;
	@Autowired private TopicRepository topicRepository;
	
	public TopicLDAInferenceAdapter(){
		refreshModel();
	}

	@Override
	public void refreshModel(){
		LDAConfigurator ldaConfigurator = LDAConfiguratorBuilder.defaults().get();
		inferencer = new Inferencer(ldaConfigurator.getThreadCnt());
		inferencer.init(ldaConfigurator);
	}	

	@Override
	public Map<Integer, Double> extractTopics(List<ExtraWord> keywords){
		//updated to collection for dynamic evolution/growth
		List<String> strs = mapToLDARaw(keywords);
		//performs inference
		TopicModel model = inferencer.infer(DEFAULT_INFERENCE_NAME,strs.toArray(new String[strs.size()]));
		Map<Integer, Double> documentTopicDistrib =  retrieveDistributionFromModel(model);
		
		// print top topics
		printTopTopics(documentTopicDistrib);
		
		
		return documentTopicDistrib;
		//mapping on persistent distribution
//		return mapRawDocumentTopicDistributionToPersistentDistrib(documentTopicDistrib);
	}

	private void printTopTopics(Map<Integer, Double> documentTopicDistrib) {
		SortedMap<Double, Integer> sortedDoc = new TreeMap<Double, Integer>();
		for(Map.Entry<Integer, Double> entry : documentTopicDistrib.entrySet()){
			sortedDoc.put(entry.getValue(), entry.getKey());
		}
		System.out.println("Sorted Topics:"+sortedDoc);
	}
	
	@Override
	public Map<Topic, Double> getTrainingDocumentTopicDistribution(LDADocument trainDocument){
		Map<Integer, Double> documentTopicDistribution = 
				inferencer.getTrainingDocumentTopicDistribution(trainDocument.getLdaTrainIdentifier());
		return mapRawDocumentTopicDistributionToPersistentDistrib(documentTopicDistribution);
	}
	
	@Override
	public Map<Integer, Double> getPlainTrainingDocumentTopicDistribution(LDADocument trainDocument) {
		return inferencer.getTrainingDocumentTopicDistribution(trainDocument.getLdaTrainIdentifier());
	}
	
	@Override
	public Map<Integer, Double> getPlainTrainingDocumentTopicDistribution(int ldaTrainIdentifier) {
		return inferencer.getTrainingDocumentTopicDistribution(ldaTrainIdentifier);
	}
	
	private Map<Topic, Double> mapRawDocumentTopicDistributionToPersistentDistrib(Map<Integer, Double> documentTopicDistrib) {
		Map<Topic, Double> topicsDistrib = new HashMap<Topic, Double>();
		for(Map.Entry<Integer, Double> entry : documentTopicDistrib.entrySet()){
			Topic t = topicRepository.findOne(TopicSpecification.hasIdentifier(entry.getKey()));
			topicsDistrib.put(t, entry.getValue());
		}
		return topicsDistrib;
	}
	
	private List<String> mapToLDARaw(List<ExtraWord> keywords) {
		List<String> strs = new ArrayList<String>();
		for(ExtraWord extraWord : keywords){
			//added to increase the weight of a keyword based on its occurrences
			for(int i=0;i<extraWord.getOccurrance();i++){
				strs.add(extraWord.getWord().getName());
			}
		}
		return strs;
	}

	private Map<Integer, Double> retrieveDistributionFromModel(TopicModel model) {
		return model.getTopicDistribution(0);
	}

}