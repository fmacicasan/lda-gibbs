package ro.consys.algorithm.service.topic.lda;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import ro.consys.algorithm.service.topic.lda.config.LDAConfigurator;

/**
 * 
 * @author Flopi
 *
 */
public class LDAProcess {
	private static Logger log = Logger.getLogger(LDAProcess.class);
	
	private static final int THREAD_CNT = 2;
	private static ExecutorService EXECUTOR = null;
	
	private ExecutorService pool;
	private int threadCount;
	
	protected TopicModel trainModel;
	protected LDAConfigurator option;
	
	public LDAProcess(){
		this(THREAD_CNT);
	}
	
	public LDAProcess(int threadCount2) {
		this(EXECUTOR,threadCount2);
	}
	
	private LDAProcess(ExecutorService pool, int threadCount){
		if(pool == null){
			//want to maintain thread pool between invocations
			pool = Executors.newFixedThreadPool(threadCount);
		}
		this.pool = pool;
		this.threadCount = threadCount;
	}
	
	protected void init(TopicModel trainModel){
		this.trainModel = trainModel;
	}

	
	public int getThreadCount(){
		return this.threadCount;
	}
	
	public Future submit(Runnable r){
		return pool.submit(r);
	}
	
	public void shutdownNow(){
		this.pool.shutdownNow();
	}
	
}
