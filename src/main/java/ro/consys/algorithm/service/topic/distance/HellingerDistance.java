package ro.consys.algorithm.service.topic.distance;

import static java.lang.Math.pow;

import java.util.List;
import java.util.Map;

import ro.consys.algorithm.domain.Topic;

public class HellingerDistance implements TopicDistance {
	
	@Override
	public double compute(List<Double> x, List<Double> y){
		//return hellinger1((Double[])x.toArray(), (Double[])y.toArray());
		return hellinger(x, y);
	}
	
	double hellinger(List<Double> x, List<Double> y) {
		double xSum = 0, ySum = 0;
		int count = x.size();
		//supposed distributions not normalized
		for (int i = 0; i < count; i++) {
			xSum += x.get(i);
			ySum += y.get(i);
		}
		double result = 0d;
		for (int i = 0; i < count; i++) {
				result += computeIntermediary(x.get(i), xSum, y.get(i), ySum);
//				result += pow(sqrt2(x.get(i) / xSum)- sqrt2(y.get(i) / ySum), 2.0);
		}
		return sqrt2(result);
	}
	@Override
	public double compute(Map<Integer, Double> x, Map<Integer, Double> y) {
		//TODO check if they have same keys
		double xSum = 0, ySum = 0;
		for (Integer i : x.keySet()) {
			xSum += x.get(i);
			ySum += y.get(i);
		}
		double result = 0d;
		for (Integer i : x.keySet()) {
			result += computeIntermediary(x.get(i), xSum, y.get(i), ySum);
//			result += pow(sqrt2(x.get(i) / xSum)- sqrt2(y.get(i) / ySum), 2.0);
		}
		return sqrt2(result);
	}
	@Override
	public double computeTopic(Map<Topic, Double> x, Map<Topic, Double> y) {
		//TODO check if they have same keys
				double xSum = 0, ySum = 0;
				for (Topic i : x.keySet()) {
					xSum += x.get(i);
					ySum += y.get(i);
				}
				double result = 0d;
				for (Topic i : x.keySet()) {
					result += computeIntermediary(x.get(i), xSum, y.get(i), ySum);
//					result += pow(sqrt2(x.get(i) / xSum)- sqrt2(y.get(i) / ySum), 2.0);
				}
				return sqrt2(result);
	}
	
	private double computeIntermediary(double xComponent, double xSum, double yComponent, double ySum){
		return pow(sqrt2(xComponent / xSum)- sqrt2(yComponent / ySum), 2.0);
	}
	
	
//	y_plus[0] = y_plus[1] = 0;
//	double y_plus[] = new double[2];	
//	if(sqrt.isNaN()){
//		System.out.println("i am nan"+sqrt);
//	}

	double hellinger1(float[][] y, int m, int n, int y1, int y2) {
		int i;
		int y_plus[] = new int[2];
		float result = 0f;

		y_plus[0] = 0;
		y_plus[1] = 0;

		for (i = 0; i < n; i++) {
			y_plus[0] += y[y1][i];
			y_plus[1] += y[y2][i];
		}

		for (i = 0; i < n; i++) {
			if (y_plus[0] * y_plus[1] != 0) {
				result += Math.pow(
						Math.sqrt(y[y1][i] / y_plus[0])- Math.sqrt(y[y2][i] / y_plus[1]), 2.0);
			}
		}

		return Math.sqrt(result);
	}
	
	double hellinger1(Double[] x, Double[] y) {
		double y_plus[] = new double[2];
		y_plus[0] = y_plus[1] = 0;
		int n = x.length;
		for (int i = 0; i < n; i++) {
			y_plus[0] += x[i];
			y_plus[1] += y[i];
		}
		double result = 0d;
		for (int i = 0; i < n; i++) {
			if (y_plus[0] * y_plus[1] != 0) {
				result += pow(sqrt2((double) x[i] / y_plus[0])- sqrt2((double) y[i] / y_plus[1]), 2.0);
			}
		}
		Double sqrt = sqrt2(result);
		if(sqrt.isNaN()){
			System.out.println("i am nan"+sqrt);
		}
		return sqrt;
	}
	
	double hellinger11(double[] x, double[] y) {
		double y_plus[] = new double[2];
		y_plus[0] = y_plus[1] = 0;
		int n = x.length;
		for (int i = 0; i < n; i++) {
			y_plus[0] += x[i];
			y_plus[1] += y[i];
		}
		double result = 0d;
		for (int i = 0; i < n; i++) {
			if (y_plus[0] * y_plus[1] != 0) {
				result += pow(sqrt2(x[i] / y_plus[0])- sqrt2(y[i] / y_plus[1]), 2.0);
			}
		}
		Double sqrt = sqrt2(result);
		if(sqrt.isNaN()){
			System.out.println("i am nan"+sqrt);
		}
		return sqrt;
	}
	
	public double sqrt2(double number){
		return Math.sqrt(Math.abs(number));
	}




}
