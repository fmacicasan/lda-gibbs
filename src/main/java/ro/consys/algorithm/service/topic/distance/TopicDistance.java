package ro.consys.algorithm.service.topic.distance;

import java.util.List;
import java.util.Map;

import ro.consys.algorithm.domain.Topic;

public interface TopicDistance{
	double compute(List<Double> x, List<Double> y);
	double compute (Map<Integer,Double> x, Map<Integer, Double> y);
	double computeTopic (Map<Topic,Double> x, Map<Topic, Double> y);
	
}
