package ro.consys.algorithm.service.topic.lda;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.log4j.Logger;

import ro.consys.algorithm.domain.Topic;
import ro.consys.algorithm.service.topic.lda.config.LDAConfigurator;
import ro.consys.algorithm.service.topic.lda.config.ModelStatus;
import ro.consys.algorithm.service.topic.lda.model.Dictionary;
import ro.consys.algorithm.service.topic.lda.model.Document;
import ro.consys.algorithm.service.topic.lda.model.Dataset;
import ro.consys.algorithm.service.topic.lda.model.Pair;
import ro.consys.algorithm.service.topic.lda.util.LDACmdOption;
import ro.consys.algorithm.util.IOUtil;
import ro.consys.algorithm.util.Log;


public class TopicModel {

	private static final Logger log = Logger.getLogger(TopicModel.class);
	
    private static String tassignSuffix;    //suffix for topic assignment file
    private static String thetaSuffix;        //suffix for theta (topic - document distribution) file
    private static String phiSuffix;        //suffix for phi file (topic - word distribution) file
    private static String othersSuffix;     //suffix for containing other parameters
    private static String twordsSuffix;        //suffix for file containing words-per-topics

    private String wordMapFile;         //file that contain word to id map
    private String trainlogFile;     //training log file

    private String dir;
    private String dfile;
    private String modelName;

    public double getEta() {
        return eta;
    }

    public double getAlpha() {
        return alpha;
    }

    public int getVocabularySize() {
        return V;
    }

    public int getSamplingIterationCount() {
        return niters;
    }

    private ModelStatus modelStatus;

    private Dataset data;            // link to a dataset

    private int M; //dataset size (i.e., number of docs)
    private int V; //vocabulary size
    private int K; //number of topics
    private double alpha; //LDA  hyperparameters
    
    //TODO instead ETA
    private double eta; //LDA  hyperparameters
    private int niters; //number of Gibbs sampling iteration
    private int liter; //the iteration at which the model was saved
    private int savestep; //saving period
    private int twords; //print out top words per each topic
    private int withrawdata;

    // Estimated/Inferenced parameters
    private double[][] theta; //theta: document - topic distributions, size M x K
    //TODO change with beta
    private double[][] phi; // phi: topic-word distributions, size K x V

    // Temp variables while sampling
    private Vector<Integer>[] z; //topic assignments for words, size M x doc.size()
    private int[][] nw; //nw[i][j]: number of instances of word/term i assigned to topic j, size V x K
    private int[][] nd; //nd[i][j]: number of words in document i assigned to topic j, size M x K
    private int[] nwsum; //nwsum[j]: total number of words assigned to topic j, size K
    private int[] ndsum; //ndsum[i]: total number of words in document i, size M 	

    public TopicModel() {
        setDefaultValues();
    }
    
    public int getTotalNumberOfWordsInTopic(int topicJ){
    	return this.nwsum[topicJ];
    }
    
    public int getTotalNumberOfWordsInDocument(int docI){
    	return this.ndsum[docI];
    }
    
    public int getNumberOfWordsAssignedToTopic(int wordI, int topicJ){
    	return this.nw[wordI][topicJ];
    }
    
    public int getNumberOfWordsInDocAssignedToTopic(int docI, int topicJ){
    	return this.nd[docI][topicJ];
    }

    public Integer getTopicOfWordInDocument(int documentM, int wordN){
    	if(documentM >= z.length){
    		//TODO validation
    	}
    	return z[documentM].get(wordN);
    }
    
    public void setTopicOfWordInDocument(int documentM, int wordN, int topic){    	
    	this.z[documentM].set(wordN, topic);
    }

   

    /**
     * Init parameters for estimation
     */
    public boolean initNewModel(LDAConfigurator option) {
        if (!init(option)){
            return false;
        }

//		p = new double[K];		

        data = Dataset.readDataSet(dir + File.separator + dfile);
        if (data == null) {
            System.out.println("Fail to read training data!\n");
            return false;
        }

        //+ allocate memory and assign values for variables
        M = data.getNumberOfDocuments();
        V = data.getNumberOfWords();
        dir = option.getDir();
        savestep = option.getSavestep();

        // K: from command line or default value
        // alpha, beta: from command line or default values
        // niters, savestep: from command line or default values

        initializeDataMatrix();

        initializeTopicsWordAssignment(true);

        initializeThetaAndPhi();

        return true;
    }
    
    public boolean initNewModel(LDAConfigurator option, Dataset inputData){
    	if(inputData == null){
    		log.error("LDADataset inputData should not be null while initializing new model!");
    		return false;
    	}
    	if (!init(option)){
            return false;
        }		

        data = inputData;

        //+ allocate memory and assign values for variables
        M = data.getNumberOfDocuments();
        V = data.getNumberOfWords();
        dir = option.getDir();
        savestep = option.getSavestep();

        // K: from command line or default value
        // alpha, beta: from command line or default values
        // niters, savestep: from command line or default values

        initializeDataMatrix();

        initializeTopicsWordAssignment(true);

        initializeThetaAndPhi();

        return true;
    }

    /**
     * Init parameters for inference
     *
     * @param newData DataSet for which we do inference
     */
    public boolean initNewModel(LDAConfigurator option, Dataset newData, TopicModel trnModel) {
        if (!init(option)){
            return false;
        }

        K = trnModel.K;
        alpha = trnModel.alpha;
        eta = trnModel.eta;

//		p = new double[K];
        System.out.println("K:" + K);

        data = newData;

        //+ allocate memory and assign values for variables
        M = data.getNumberOfDocuments();
        V = data.getNumberOfWords();
//        dir = option.dir;
//        savestep = option.savestep;
        initDirAndSaveStep(option);

        System.out.println("M:" + M);
        System.out.println("V:" + V);

        // K: from command line or default value
        // alpha, beta: from command line or default values
        // niters, savestep: from command line or default values

        initializeDataMatrix();

        initializeTopicsWordAssignment(true);

        initializeThetaAndPhi();

        return true;
    }

	private void initializeThetaAndPhi() {
		theta = new double[M][K];
        phi = new double[K][V];
	}

    /**
     * Init parameters for inference
     * reading new dataset from file
     */
    public boolean initNewModel(LDAConfigurator option, TopicModel trnModel) {
        if (!init(option))
            return false;

        Dataset dataset = Dataset.readDataSet(dir + File.separator + dfile, trnModel.data.getLocalDictionary());
        if (dataset == null) {
            System.out.println("Fail to read dataset!\n");
            return false;
        }

        return initNewModel(option, dataset, trnModel);
    }

    /**
     * init parameter for continue estimating or for later inference
     */
    public boolean initEstimatedModel(LDAConfigurator option) {
        if (!init(option))
            return false;

        if (!loadModel()) {
            System.out.println("Fail to load word-topic assignment file of the model!\n");
            return false;
        }

        initDirAndSaveStep(option);

        Log.info(log, getInfo());

        initializeDataMatrix();
        
        initializeTopicsWordAssignment(false);

        initializeThetaAndPhi();


        return true;
    }

	private void initDirAndSaveStep(LDAConfigurator option) {
		dir = option.getDir();
        savestep = option.getSavestep();
	}
    
	@SuppressWarnings("unchecked")
	private void initializeTopicsWordAssignment(boolean createTopics) {
		if(createTopics){
			z = new Vector[M];
		}
        for (int m = 0; m < data.getNumberOfDocuments(); m++) {
            int N = data.getDocLength(m);
            if(createTopics){
            	z[m] = new Vector<Integer>();
            }

            //initilize for z
            for (int n = 0; n < N; n++) {
                int topic = (createTopics)?(int) Math.floor(Math.random() * K):(Integer)z[m].get(n);
                if(createTopics){
                	z[m].add(topic);
                }

                // number of instances of word assigned to topic j
                nw[data.getWordInDocument(m, n)][topic] += 1;
                // number of words in document i assigned to topic j
                nd[m][topic] += 1;
                // total number of words assigned to topic j
                nwsum[topic] += 1;
            }
            // total number of words in document i
            ndsum[m] = N;
        }
	}

    private void initializeDataMatrix() {
        int m, w, k;
        nw = new int[V][K];
        for (w = 0; w < V; w++) {
            for (k = 0; k < K; k++) {
                nw[w][k] = 0;
            }
        }

        nd = new int[M][K];
        for (m = 0; m < M; m++) {
            for (k = 0; k < K; k++) {
                nd[m][k] = 0;
            }
        }

        nwsum = new int[K];
        for (k = 0; k < K; k++) {
            nwsum[k] = 0;
        }

        ndsum = new int[M];
        for (m = 0; m < M; m++) {
            ndsum[m] = 0;
        }
    }

    public String getInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("Model Info:");
        sb.append("\talpha:" + alpha);
        sb.append("\tbeta:" + eta);
        sb.append("\tM:" + M);
        sb.append("\tV:" + V);
        return sb.toString();
    }

    public Dataset getLDADataset() {
        return data;
    }

    public int getDocLength(int document) {
        return data.getDocLength(document);
    }

    public Dictionary getLocalDictionary() {
        return data.getLocalDictionary();
    }

    public int getWordInDocument(int doc, int word) {
        return data.getWordInDocument(doc, word);
    }

    public int getGlobalId(int _w) {
        return data.getGlobalId(_w);
    }

    public int getDatasetSize() {
        return M;
    }

    public int getSavingIterationCount() {
        return liter;
    }

    public void setSavingIterationCount(int newSavingIterationCount){
        this.liter = newSavingIterationCount;
    }

    public void initializeSavingIterationCount(){
        this.liter = 1;
    }

    public void incrementSavingIterationCount(){
        this.liter ++;
    }

    public void decrementSavingIterationCount(){
        this.liter --;
    }

    public int getTopicCount() {
        return K;
    }

	public Map<Integer, Double> getTopicDistribution(int document) {
		Map<Integer, Double> documentTopicDistrib = new HashMap<Integer, Double>();
		for(int i=0;i<K;i++){
			documentTopicDistrib.put(i, theta[document][i]);
		}
		return documentTopicDistrib;
	}
	
	/**
	 * removeTopicUnderSamplingFromCountVars
	 * @param documentM
	 * @param topic
	 * @param word_w
	 */
	public void removeOldTopicFromCountables(int documentM, int topic, int word_w) {
//		newModel.nw[_w][topic] -= 1;
		nd[documentM][topic] -= 1; //document dependent
		nwsum[topic] -= 1;
		ndsum[documentM] -= 1;//document dependent
	}
	/**
	 * addNewlyEstimatedTopicToCountVars
	 * @param documentM
	 * @param topic
	 * @param word_w
	 */
	public void addNewTopicToCountables(int documentM, int topic, int word_w) {
		// add newly estimated z_i to count variables
		nw[word_w][topic] += 1;
		nd[documentM][topic] += 1;
		nwsum[topic]++;
		ndsum[documentM]++;
	}
	
	
	/************************************************************* from initial [27] implementation **************************************/
	
	public void computePhi() {
		for (int k = 0; k < this.getTopicCount(); k++) {
			for (int w = 0; w < this.getVocabularySize(); w++) {
				this.phi[k][w] = (this.getNumberOfWordsAssignedToTopic(w, k) + this.getEta())/ (this.getTotalNumberOfWordsInTopic(k) + this.getVocabularySize() * this.getEta());
			}
		}
	}
	
	public void computeTheta() {
		for (int m = 0; m < getDatasetSize(); m++) {
			for (int k = 0; k < getTopicCount(); k++) {
				theta[m][k] = (getNumberOfWordsInDocAssignedToTopic(m,k)+ getAlpha())/ (getTotalNumberOfWordsInDocument(m) + getTopicCount() * getAlpha());
			}
		}
	}
	
	protected void computeNewTheta(){
		for (int m = 0; m < getDatasetSize(); m++){
			for (int k = 0; k < getTopicCount(); k++){
				theta[m][k] = (getNumberOfWordsInDocAssignedToTopic(m, k)+ getAlpha()) / (getTotalNumberOfWordsInDocument(m)+ getTopicCount() * getAlpha());
			}
		}
	}
	
	protected void computeNewPhi(TopicModel trnModel){
		for (int k = 0; k < getTopicCount(); k++){
			for (int _w = 0; _w < getVocabularySize(); _w++){
				Integer id = getGlobalId(_w);
				
				if (id != null){
					phi[k][_w] = (trnModel.getNumberOfWordsAssignedToTopic(id, k) + getNumberOfWordsAssignedToTopic(_w, k)
							+ getEta()) / (getTotalNumberOfWordsInTopic(k) + getTotalNumberOfWordsInTopic(k) + trnModel.getVocabularySize() * getEta());
				}
			}
		}
	}
	
    /**
     * read other file to get parameters
     */
    protected boolean readOthersFile(String otherFile) {
        //open file <model>.others to read:

        try {

        	BufferedReader reader = new BufferedReader(new InputStreamReader(IOUtil.getResourceAsStream(otherFile)));
            String line;
            while ((line = reader.readLine()) != null) {
                StringTokenizer tknr = new StringTokenizer(line, "= \t\r\n");

                int count = tknr.countTokens();
                if (count != 2)
                    continue;

                String optstr = tknr.nextToken();
                String optval = tknr.nextToken();

                if (optstr.equalsIgnoreCase("alpha")) {
                    alpha = Double.parseDouble(optval);
                } else if (optstr.equalsIgnoreCase("beta")) {
                    eta = Double.parseDouble(optval);
                } else if (optstr.equalsIgnoreCase("ntopics")) {
                    K = Integer.parseInt(optval);
                } else if (optstr.equalsIgnoreCase("liter")) {
                    liter = Integer.parseInt(optval);
                } else if (optstr.equalsIgnoreCase("nwords")) {
                    V = Integer.parseInt(optval);
                } else if (optstr.equalsIgnoreCase("ndocs")) {
                    M = Integer.parseInt(optval);
                } else {
                    // any more?
                }
            }

            reader.close();
        } catch (Exception e) {
            System.out.println("Error while reading other file:" + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    protected boolean readTAssignFile(String tassignFile) {
        try {
            int i, j;
            BufferedReader reader = new BufferedReader(new InputStreamReader(
            			IOUtil.getResourceAsStream(tassignFile), "UTF-8"));

            String line;
            z = new Vector[M];
            data = new Dataset(M);
            data.setNumberOfWords(V);
            for (i = 0; i < M; i++) {
                line = reader.readLine();
                StringTokenizer tknr = new StringTokenizer(line, " \t\r\n");

                int length = tknr.countTokens();

                Vector<Integer> words = new Vector<Integer>();
                Vector<Integer> topics = new Vector<Integer>();

                for (j = 0; j < length; j++) {
                    String token = tknr.nextToken();

                    StringTokenizer tknr2 = new StringTokenizer(token, ":");
                    if (tknr2.countTokens() != 2) {
                        System.out.println("Invalid word-topic assignment line\n");
                        return false;
                    }

                    words.add(Integer.parseInt(tknr2.nextToken()));
                    topics.add(Integer.parseInt(tknr2.nextToken()));
                }//end for each topic assignment

                //allocate and add new document to the corpus
                Document doc = new Document(words);
                data.setDoc(doc, i);

                //assign values for z
                z[i] = new Vector<Integer>();
                for (j = 0; j < topics.size(); j++) {
                    z[i].add(topics.get(j));
                }

            }//end for each doc

            reader.close();
        } catch (Exception e) {
            System.out.println("Error while loading model: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * load saved model
     */
    public boolean loadModel() {

        if (!readOthersFile(dir + File.separator + modelName + othersSuffix))
            return false;

        if (!readTAssignFile(dir + File.separator + modelName + tassignSuffix))
            return false;


        // read dictionary
        Dictionary dict = new Dictionary();
        if (!dict.readWordMap(dir + File.separator + wordMapFile))
            return false;

        data.setLocalDictionary(dict);

        return true;
    }

    /**
     * Save word-topic assignments for this model
     */
    public boolean saveModelTAssign(String filename) {
        int i, j;

        try {
        	IOUtil.createFilePath(filename);
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));

            //write docs with topic assignments for words
            for (i = 0; i < data.getNumberOfDocuments(); i++) {
                for (j = 0; j < data.getDocLength(i); ++j) {
//					writer.write(data.docs[i].getWord(j) + ":" + z[i].get(j) + " ");					
                    writer.write(data.getWordInDocument(i, j) + ":" + z[i].get(j) + " ");
                }
                writer.write("\n");
            }

            writer.close();
        } catch (Exception e) {
            System.out.println("Error while saving model tassign: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * Save theta (topic distribution) for this model
     */
    public boolean saveModelTheta(String filename) {
        try {
        	IOUtil.createFilePath(filename);
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
            for (int i = 0; i < M; i++) {
                for (int j = 0; j < K; j++) {
                    writer.write(theta[i][j] + " ");
                }
                writer.write("\n");
            }
            writer.close();
        } catch (Exception e) {
            System.out.println("Error while saving topic distribution file for this model: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Save word-topic distribution
     */

    public boolean saveModelPhi(String filename) {
        try {
        	IOUtil.createFilePath(filename);
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));

            for (int i = 0; i < K; i++) {
                for (int j = 0; j < V; j++) {
                    writer.write(phi[i][j] + " ");
                }
                writer.write("\n");
            }
            writer.close();
        } catch (Exception e) {
            System.out.println("Error while saving word-topic distribution:" + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Save other information of this model
     */
    public boolean saveModelOthers(String filename) {
        try {
        	IOUtil.createFilePath(filename);
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));

            writer.write("alpha=" + alpha + "\n");
            writer.write("beta=" + eta + "\n");
            writer.write("ntopics=" + K + "\n");
            writer.write("ndocs=" + M + "\n");
            writer.write("nwords=" + V + "\n");
            writer.write("liters=" + liter + "\n");

            writer.close();
        } catch (Exception e) {
            System.out.println("Error while saving model others:" + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Save model the most likely words for each topic
     */
    public boolean saveModelTwords(String filename) {
        try {
        	IOUtil.createFilePath(filename);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(filename), "UTF-8"));

            if (twords > V) {
                twords = V;
            }

            for (int k = 0; k < K; k++) {
                List<Pair> wordsProbsList = new ArrayList<Pair>();
                for (int w = 0; w < V; w++) {
                    Pair p = new Pair(w, phi[k][w], false);

                    wordsProbsList.add(p);
                }//end foreach word

                //print topic
                writer.write("Topic " + k + "th:\n");
                Collections.sort(wordsProbsList);

                for (int i = 0; i < twords; i++) {
                    if (data.getLocalDictionary().contains((Integer) wordsProbsList.get(i).getFirst())) {
                        String word = data.getLocalDictionary().getWord((Integer) wordsProbsList.get(i).getFirst());

                        writer.write("\t" + word + " " + wordsProbsList.get(i).getSecond() + "\n");
                    }
                }
            } //end foreach topic

            writer.close();
        } catch (Exception e) {
            System.out.println("Error while saving model twords: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean saveModel() {
        return saveModel(dfile + "." + modelName);
    }

    /**
     * Save model
     */
    public boolean saveModel(String modelName) {
        if (!saveModelTAssign(dir + File.separator + modelName + tassignSuffix)) {
            return false;
        }

        if (!saveModelOthers(dir + File.separator + modelName + othersSuffix)) {
            return false;
        }

        if (!saveModelTheta(dir + File.separator + modelName + thetaSuffix)) {
            return false;
        }

        if (!saveModelPhi(dir + File.separator + modelName + phiSuffix)) {
            return false;
        }

        if (twords > 0) {
            if (!saveModelTwords(dir + File.separator + modelName + twordsSuffix))
                return false;
        }
        return true;
    }
    
    /**
     * Set default values for variables
     */
    private void setDefaultValues() {
        wordMapFile = "wordmap.txt";
        trainlogFile = "trainlog.txt";
        tassignSuffix = ".tassign";
        thetaSuffix = ".theta";
        phiSuffix = ".phi";
        othersSuffix = ".others";
        twordsSuffix = ".twords";

        dir = "./";
        dfile = "trndocs.dat";
        modelName = "model-final";
        modelStatus = ModelStatus.UNKNOWN;

        M = 0;
        V = 0;
        K = 100;
        alpha = 50.0 / K;
        eta = 0.1;
        niters = 2000;
        liter = 0;

        z = null;
        nw = null;
        nd = null;
        nwsum = null;
        ndsum = null;
        theta = null;
        phi = null;
    }
    
    protected boolean init(LDAConfigurator option) {
        if (option == null){
            return false;
        }

        modelName = option.getModelName();
        K = option.getK();

        alpha = option.getAlpha();
        if (alpha < 0.0)
            alpha = 50.0 / K;

        if (option.getBeta() >= 0)
            eta = option.getBeta();

        niters = option.getNiters();

        dir = option.getDir();
        if (dir.endsWith(File.separator))
            dir = dir.substring(0, dir.length() - 1);

        dfile = option.getDfile();
        twords = option.getTwords();
        wordMapFile = option.getWordMapFileName();

        return true;
    }

}
