package ro.consys.algorithm.service;

import java.util.List;
import java.util.Map;

import ro.consys.algorithm.domain.ExtraWord;
import ro.consys.algorithm.domain.LDADocument;
import ro.consys.algorithm.domain.Topic;

public interface TopicIdentifierService {

	/**
	 * Synchronize LDA model with data source
	 */
	public void refreshModel();

	/**
	 * Identifies topic distribution for a collection of keywords
	 * @param keywords collection of keywords
	 * @return mapping on persistent topics
	 */
	public Map<Integer, Double> extractTopics(List<ExtraWord> keywords);

	/**
	 * Retrieves topic distribution of document that took part in
	 * the training process
	 * @param trainDocument lda training identifier
	 * @return mapping on persistent topics
	 */
	public Map<Topic, Double> getTrainingDocumentTopicDistribution(LDADocument trainDocument);
	
	//TODO reconsider this to a List rather than map
	public Map<Integer, Double> getPlainTrainingDocumentTopicDistribution(LDADocument trainDocument);

	Map<Integer, Double> getPlainTrainingDocumentTopicDistribution(int ldaTrainIdentifier);

}