package ro.consys.algorithm.domain.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import ro.consys.algorithm.domain.Topic;

public class TopicSpecification {
	public static Specification<Topic> hasIdentifier(final Integer id)
	{
		return new Specification<Topic>() {
			@Override
			public Predicate toPredicate(Root<Topic> root, CriteriaQuery<?> query, CriteriaBuilder cb)
			{
				return cb.equal(root.<Integer> get("identifier"), id);
			}
		};
	}
}
