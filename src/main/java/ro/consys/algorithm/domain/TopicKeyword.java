package ro.consys.algorithm.domain;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class TopicKeyword
{
	@ManyToOne
	@JoinColumn
	private Topic topic;

	@ManyToOne
	@JoinColumn
	private Word keyword;

	private double relevance;
}
