package ro.consys.algorithm.domain;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

public class Topic
{
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "topic")
	private Set<TopicKeyword> keywords = new HashSet<TopicKeyword>();

	/**
	 * Identifies the topic within the series of topics (from 1 to K - topic count)
	 */
	private int identifier;

	/**
	 * Auxiliary information about topic structure / components.
	 */
	private String description;
}
