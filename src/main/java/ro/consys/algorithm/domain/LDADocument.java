package ro.consys.algorithm.domain;

public interface LDADocument {
	public static final int DEFAULT_NOT_USED_IN_LDA_TRAIN_IDENTIFIER = -1;

	public int getLdaTrainIdentifier();

}
