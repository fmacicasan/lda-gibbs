package ro.consys.algorithm.domain;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

public class ClassifierModel
{
	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date timestamp;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] binaryData;
}
