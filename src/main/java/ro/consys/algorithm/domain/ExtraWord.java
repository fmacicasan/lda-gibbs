package ro.consys.algorithm.domain;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class ExtraWord
{
	@ManyToOne
	@JoinColumn
	private Word word;

	private boolean train; // true => inregistrare folosita la antrenarea clasificatorului

	@ManyToOne
	@JoinColumn
	private ClassifierModel model;

	private boolean keyword;
	private double frequency;
	private int occurrance;

	private boolean inHeading; // <h1>...</h1>
	private boolean inTitle; // <title>...</title>
	private boolean inMetaKeywords; // <meta name="keywords" content="...">
	private boolean inMetaDescription; // <meta name="description" content="...">

	private boolean inPageUrl; // http://...

	private boolean inLinkName; // <a href=" ">...</a>
	private boolean inImageAlt; // <img alt="..." />

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(word.toString());
		sb.append(" ,").append(((frequency < 0) ? "?" : frequency));
		sb.append(" ,").append(((inHeading) ? 1 : 0));
		sb.append(" ,").append(((inTitle) ? 1 : 0));
		sb.append(" ,").append(((inMetaKeywords) ? 1 : 0));
		sb.append(" ,").append(((inMetaDescription) ? 1 : 0));
		sb.append(" ,").append(((inPageUrl) ? 1 : 0));
		sb.append(" ,").append(((inLinkName) ? 1 : 0));
		sb.append(" ,").append(((inImageAlt) ? 1 : 0));
		return sb.toString();
	}

	public void promoteToKeyword()
	{
		this.keyword = true;
	}

	public static final String ATTRIBUTE_FREQUENCY = "frequency";
	public static final String ATTRIBUTE_IS_KEYWORD = "isKeyword";
	public static final String ATTRIBUTE_IN_HEADING = "inHeading";
	public static final String ATTRIBUTE_IN_TITLE = "inTitle";
	public static final String ATTRIBUTE_IN_META_KEYWORDS = "inMetaKeywords";
	public static final String ATTRIBUTE_IN_META_DESCRIPTION = "inMetaDescription";
	public static final String ATTRIBUTE_IN_PAGE_URL = "inPageUrl";
	public static final String ATTRIBUTE_IN_LINK_NAME = "inLinkName";
	public static final String ATTRIBUTE_IN_IMAGE_ALT = "inImageAlt";
	public static final String ATTRIBUTE_YES = "yes";
	public static final String ATTRIBUTE_NO = "no";

	public int getOccurrance() {
		return occurrance;
	}

	public Word getWord() {
		return word;
	}
}
