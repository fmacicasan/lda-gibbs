package ro.consys.algorithm.domain;

import javax.persistence.Column;

public class Word
{
	public static final Integer DEFAULT_NO_DICTIONARY_VALUE = -1;

	@Column(unique = true)
	private String name;
	private boolean isStopWord;

	private Integer dictionaryKey;

	public String getName() {
		return name;
	}

	public void setDictionaryKey(int dictionaryKey) {
		this.dictionaryKey = dictionaryKey;
	}
}
