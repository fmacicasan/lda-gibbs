package ro.consys.algorithm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AlgorithmUtil
{
	public static String readAll(InputStream is) throws IOException
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = reader.readLine();

		while (line != null)
		{
			sb.append(line);
			sb.append("\n");
			line = reader.readLine();
		}
		return sb.toString();
	}
}
