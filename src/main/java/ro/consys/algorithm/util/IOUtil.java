package ro.consys.algorithm.util;

import java.io.File;
import java.io.InputStream;

public class IOUtil {
	public static void createFilePath(String filename) {
		File file = new File(filename);
		if(!file.getParentFile().exists()){
			file.getParentFile().mkdirs();
		}
	}
	
	public static String preparePathForResourceGather(String filename){
		return filename.replace("src/main/resources", "");
	}
	
	public static InputStream getResourceAsStream(String filename){
		System.out.println(filename);
		return IOUtil.class.getResourceAsStream(preparePathForResourceGather(filename));
	}
}
