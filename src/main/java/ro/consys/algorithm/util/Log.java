package ro.consys.algorithm.util;

import org.apache.log4j.Logger;

/**
 * Used for logging purpose. 
 * Call its methods to push back the concatenation of messages to the 
 * case when that actual logging level is enabled (info/debug).
 * 
 * @author Flopi
 *
 */
public class Log {
	
	public static void info(Logger log, Object... msgs){
		if(log.isInfoEnabled()){
			String message = constructMessage(msgs);
			log.info(message);
		}
	}
	
	public static void debug(Logger log, Object... msgs){
		if(log.isDebugEnabled()){
			String message = constructMessage(msgs);
			log.debug(message);
		}
	}
	
	public static void error(Logger log, Throwable throwable, Object... msgs){
		String message = constructMessage(msgs);
		log.error(message, throwable);
	}
	
	public static void error(Logger log, Object... msgs){
		String message = constructMessage(msgs);
		log.error(message);
	}
	
	
	
	private static String constructMessage(Object... msgs){
		StringBuilder sb = new StringBuilder();
		for(Object object: msgs){
			sb.append(object).append(" ");
		}
		return sb.toString();
	}

}
