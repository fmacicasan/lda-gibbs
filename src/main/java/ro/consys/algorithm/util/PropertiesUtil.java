package ro.consys.algorithm.util;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import ro.consys.algorithm.service.topic.lda.config.LDAConfiguratorBuilder;

public class PropertiesUtil {
	
    private static final Logger log = Logger.getLogger(PropertiesUtil.class);

    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("application");

    public static String getMessage(String key) {
        return resourceBundle.getString(key);
    }
    
    public static long getLong(String key) {
        try {
            String str = resourceBundle.getString(key);
            return Long.parseLong(str);
        } catch (NumberFormatException e) {
            log.error("Invalid key for long!", e);
            //throw new InvalidArgumentException("Illegal key for long argument!");
        }
        return 0l;
    }

    public static boolean getBoolean(String key) {
        String str = resourceBundle.getString(key);
        return Boolean.parseBoolean(str);
    }

    public static int getInteger(String key) {
        try {
            String str = resourceBundle.getString(key);
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            log.error("Invalid key for long!", e);
            //throw new InvalidArgumentException("Illegal key for long argument!");
        }
        return 0;
    }
    
    public static double getDouble(String key){
    	try {
            String str = resourceBundle.getString(key);
            return Double.parseDouble(str);
        } catch (NumberFormatException e) {
            log.error("Invalid key for double!", e);
            //throw new InvalidArgumentException("Illegal key for long argument!");
        }
        return 0;
    }
    //TODO constants used, should be moved some other place
    public static final String EST_NAME = "est";
    public static final String INF_NAME = "inf";
    public static final String ESTC_NAME = "estc";
    //properties keys
    public static final String LDA_INF_MODEL_TYPE = "lda.inf.model.type";
    public static final String LDA_INF_DIR = "lda.inf.dir";
    public static final String LDA_INF_DATA_FILE = "lda.inf.dataFile";
    public static final String LDA_INF_MODEL_NAME = "lda.inf.modelName";
    public static final String LDA_INF_HP_ALHPA = "lda.inf.hp.alpha";
    public static final String LDA_INF_HP_BETA = "lda.inf.hp.beta";
    public static final String LDA_INF_TOPIC_CNT = "lda.inf.topic.cnt";
    public static final String LDA_INF_ITERATION_CNT = "lda.inf.iteration.cnt";
    public static final String LDA_INF_SAVE_STEP = "lda.inf.save.step";
    public static final String LDA_INF_TOP_WORDS = "lda.inf.top.words";
    public static final String LDA_INF_WITHDRAW_DATA = "lda.inf.withdraw.data";
    public static final String LDA_INF_WORDMAP_NAME = "lda.inf.wordmap.name";
    public static final String LDA_INF_NO_PRINT = "lda.inf.no.print";
    public static final String LDA_INF_DO_PARALLEL = "lda.inf.do.parallel";
    public static final String LDA_INF_THREAD_CNT = "lda.inf.thread.cnt";
    public static final String LDA_INF_OVERALL_REPETITIONS = "lda.inf.overall.repetitions";
    public static final String LOG_CANDIDATES = "log.candidates";
    public static final String LOG_KEYWORDS = "log.keywords";
    
    public static final String HTTP_PROXY_HOST = "http.proxyHost";
    public static final String HTTP_PROXY_PORT = "http.proxyPort";
    
    public static final String SERVER_PATH = "server.path";
    
    public static void main(String[] args){
    	LDAConfiguratorBuilder.defaults().get();
    }
    
}
