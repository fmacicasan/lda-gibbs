package ro.consys.algorithm.exception;

import com.mongodb.MongoException;


public class ConsysMongoException extends MongoException {

	public ConsysMongoException(int code, String msg) {
		super(code, msg);
		// TODO Auto-generated constructor stub
	}
	
	public ConsysMongoException(String msg){
		super(msg);
	}
	
}
