package ro.consys.algorithm.exception;

@SuppressWarnings("serial")
public class ExtracterException extends AlgorithmException {

	public ExtracterException() {
		super();
	}

	public ExtracterException(String msg) {
		super(msg);
	}

	public ExtracterException(Throwable throwable) {
		super(throwable);
	}

	public ExtracterException(String msg, Throwable throwable) {
		super(msg, throwable);
	}

}
