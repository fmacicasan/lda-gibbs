package ro.consys.algorithm.exception;

@SuppressWarnings("serial")
public class ClassifierException extends AlgorithmException {

	public ClassifierException() {
		super();
	}

	public ClassifierException(String msg) {
		super(msg);
	}

	public ClassifierException(Throwable throwable) {
		super(throwable);
	}

	public ClassifierException(String msg, Throwable throwable) {
		super(msg, throwable);
	}

}
