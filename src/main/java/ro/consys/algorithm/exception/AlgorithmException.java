package ro.consys.algorithm.exception;

@SuppressWarnings("serial")
public class AlgorithmException extends Exception {

	public AlgorithmException() {
		super();
	}

	public AlgorithmException(String msg) {
		super(msg);
	}

	public AlgorithmException(Throwable throwable) {
		super(throwable);
	}

	public AlgorithmException(String msg, Throwable throwable) {
		super(msg, throwable);
	}

}
