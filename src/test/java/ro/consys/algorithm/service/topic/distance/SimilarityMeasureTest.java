package ro.consys.algorithm.service.topic.distance;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import ro.consys.algorithm.service.topic.distance.SimilarityMesurer;


public class SimilarityMeasureTest {

	@Test
	public void test_readMatrixFromFile() throws IOException{
		SimilarityMesurer sm = new SimilarityMesurer();
		double[][] expectedResult = new double[][] {
				{ 0.0, 1.4142135623730951, 0.16971411555120366 },
				{ 1.4142135623730951, 0.0, 1.4142135623730951 },
				{ 0.16971411555120366, 1.4142135623730951, 0.0 } };
		int n = 3;
		double[][] result = sm.readDoubleMatrixFromFile("src/test/resources/matrix.txt", 3, 3);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(result[i][j] + " ");
			}
			System.out.println();
		}
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				assertTrue(Math.abs(expectedResult[i][j] - result[i][j]) < HellingerDistanceTest.THREASHOLD);
			}
		}

		
		
	}
}
