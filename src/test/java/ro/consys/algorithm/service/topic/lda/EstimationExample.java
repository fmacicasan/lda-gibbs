package ro.consys.algorithm.service.topic.lda;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ro.consys.algorithm.service.topic.TopicLDAEstimationAdapter;
import ro.consys.algorithm.service.topic.TopicSynchronizationService;
import ro.consys.algorithm.service.topic.lda.config.LDAConfigurator;
import ro.consys.algorithm.service.topic.lda.config.LDAConfiguratorBuilder;
import ro.consys.algorithm.service.topic.lda.config.ModelStatus;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class EstimationExample {
	
	private long start;
	

	private TopicSynchronizationService estimator = new TopicLDAEstimationAdapter();
	
	@Before
	public void setUp(){
		start = System.currentTimeMillis();
	}

	@After
	public void tearDown() {
		System.out.println("Processing took: " + (System.currentTimeMillis() - start) + " ms.");
	}
	
	@Test
	public void updateDictionaryKeyToDefaultTest() throws IOException, URISyntaxException {
		AtomicInteger atomicInteger = new AtomicInteger(-1);
		Map<Integer, List<String>> collect =
				Files.lines(
						Paths.get(
								EstimationExample.class.getResource("/newdocs.dat")
										.toURI())
				).map(i -> i.split(" "))
				.map(i -> Arrays.stream(i).collect(Collectors.toList()))
				.collect(Collectors.groupingBy(i -> atomicInteger.incrementAndGet()))
				.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey, i -> i.getValue().get(0)));

		//TODO modify config if needed
		LDAConfigurator ldaConfigurator = LDAConfiguratorBuilder.defaults()
				.setModelType(ModelStatus.EST).get();
		estimator.estimateFromScratchRaw(ldaConfigurator, collect);

	}

}
