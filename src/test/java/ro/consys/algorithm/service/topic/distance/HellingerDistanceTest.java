package ro.consys.algorithm.service.topic.distance;

import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;


public class HellingerDistanceTest {

	public static final double THREASHOLD = 0.000001;

	@Test
	public void testHellingerDistance() {
		HellingerDistance distance = new HellingerDistance();

		float[][] source = new float[][] { { 0, 1, 1 }, { 1, 0, 0 }, { 0, 4, 8 } };
		double[][] expectedResult = new double[][] {
				{ 0.0, 1.4142135623730951, 0.16971411555120366 },
				{ 1.4142135623730951, 0.0, 1.4142135623730951 },
				{ 0.16971411555120366, 1.4142135623730951, 0.0 } };
		double[][] result = new double[3][3];
		int n = 3;

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				result[i][j] = distance.hellinger1(source, n, n, i, j);
			}
		}

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				assertTrue(Math.abs(expectedResult[i][j] - result[i][j]) < THREASHOLD);
			}
		}

//		for (int i = 0; i < n; i++) {
//			for (int j = 0; j < n; j++) {
//				System.out.print(result[i][j] + " ");
//			}
//			System.out.println();
//		}
	}
	
	@Test
	public void testHellingerDistance2() {
		HellingerDistance distance = new HellingerDistance();

		double[][] source = new double[][] { { 0, 1, 1 }, { 1, 0, 0 }, { 0, 4, 8 } };
		double[][] expectedResult = new double[][] {
				{ 0.0, 1.4142135623730951, 0.16971411555120366 },
				{ 1.4142135623730951, 0.0, 1.4142135623730951 },
				{ 0.16971411555120366, 1.4142135623730951, 0.0 } };
		double[][] result = new double[3][3];
		int n = 3;

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				result[i][j] = distance.hellinger11(source[i],source[j]);
			}
		}
//		for (int i = 0; i < n; i++) {
//			for (int j = 0; j < n; j++) {
//				System.out.print(result[i][j] + " ");
//			}
//			System.out.println();
//		}

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				assertTrue(Math.abs(expectedResult[i][j] - result[i][j]) < THREASHOLD);
			}
		}


	}
	
	@Test
	public void testHellingerDistance3() {
		HellingerDistance distance = new HellingerDistance();
		//TODO test map;
//		Map<Integer, Double> source = new HashMap<Integer, Double>{{ 0, 1, 1 }, { 1, 0, 0 }, { 0, 4, 8 }};
//		double[][] expectedResult = new double[][] {
//				{ 0.0, 1.4142135623730951, 0.16971411555120366 },
//				{ 1.4142135623730951, 0.0, 1.4142135623730951 },
//				{ 0.16971411555120366, 1.4142135623730951, 0.0 } };
//		double[][] result = new double[3][3];
//		int n = 3;
//
//		for (int i = 0; i < n; i++) {
//			for (int j = 0; j < n; j++) {
//				result[i][j] = distance.hellinger11(source[i],source[j]);
//			}
//		}
////		for (int i = 0; i < n; i++) {
////			for (int j = 0; j < n; j++) {
////				System.out.print(result[i][j] + " ");
////			}
////			System.out.println();
////		}
//
//		for (int i = 0; i < n; i++) {
//			for (int j = 0; j < n; j++) {
//				assertTrue(Math.abs(expectedResult[i][j] - result[i][j]) < THREASHOLD);
//			}
//		}


	}
}
